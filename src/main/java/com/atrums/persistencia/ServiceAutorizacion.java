package com.atrums.persistencia;

import org.apache.log4j.Logger;

import com.atrums.modelo.ConfService;
import com.atrums.modelo.SRI.SRIDocumentoAutorizado;

public class ServiceAutorizacion {
	static final Logger log = Logger.getLogger(ServiceAutorizacion.class);
	private ConfService confService;
	private String ambiente;
	private String claveAcceso;
	
	public ServiceAutorizacion(ConfService confService, String ambiente, String claveAcceso){
		this.confService = confService;
		this.ambiente = ambiente;
		this.claveAcceso = claveAcceso;
	}

	public SRIDocumentoAutorizado CallAutorizado(){
		ServicioSRICall sriCall = new ServicioSRICall(this.confService, 
				this.ambiente, 
				null, 
				this.claveAcceso);
		
		SRIDocumentoAutorizado autorizado = new SRIDocumentoAutorizado(sriCall.AutorizadoCall());
		
		sriCall = null;
		
		return autorizado;
	}
}
