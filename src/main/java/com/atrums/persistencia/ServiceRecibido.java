package com.atrums.persistencia;

import org.apache.log4j.Logger;

import com.atrums.modelo.ConfService;
import com.atrums.modelo.SRI.SRIDocumentoRecibido;

public class ServiceRecibido {
	static final Logger log = Logger.getLogger(ServiceRecibido.class);
	private ConfService confService;
	private String ambiente;
	private String fileString;
	
	public ServiceRecibido(ConfService confService, String ambiente, String fileString){
		this.confService = confService;
		this.ambiente = ambiente;
		this.fileString = fileString;
	}
	
	public SRIDocumentoRecibido CallRecibido(){
		ServicioSRICall sriCall = new ServicioSRICall(this.confService, 
				this. ambiente, 
				this.fileString, 
				null);
		
		SRIDocumentoRecibido recibido = new SRIDocumentoRecibido(sriCall.RecibidoCall());
		
		sriCall = null;
		
		return recibido;
	}
}
