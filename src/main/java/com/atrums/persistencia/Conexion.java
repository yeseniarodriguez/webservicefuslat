package com.atrums.persistencia;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.apache.tomcat.dbcp.dbcp.BasicDataSource;

import com.atrums.modelo.Configuracion;

@SuppressWarnings("deprecation")
public class Conexion {
	static final Logger log = Logger.getLogger(Conexion.class);
	private static Configuracion configuracion = new Configuracion();
	private static DataSource dataSource = null;
	
	static {
		log.info("Inside Database() static method");
		BasicDataSource basicDataSource = new BasicDataSource();
		
		try {
			basicDataSource.setDriverClassName("org.postgresql.Driver");
			basicDataSource.setUsername(configuracion.getUsuarioservidor());
			basicDataSource.setPassword(configuracion.getPasswordservidor());
			basicDataSource.setUrl("jdbc:postgresql://" + 
					configuracion.getIpsevidor() + ":" + 
					configuracion.getPuertoservidor() + "/" + 
					configuracion.getBddsevidor());
			basicDataSource.setMaxActive(12);
			basicDataSource.setMaxIdle(12);
			basicDataSource.setInitialSize(3);
			basicDataSource.setMaxWait(10000);
			basicDataSource.setRemoveAbandonedTimeout(1500);
			
			Conexion.dataSource = basicDataSource;
		} catch (Exception ex) {log.warn(ex.getMessage());}
	}

	public static DataSource getDataSource() {
		return Conexion.dataSource;
	}

	public static void setDataSource(DataSource dataSource) {
		Conexion.dataSource = dataSource;
	}
	
	public static void createNewDataSource() throws SQLException {
		BasicDataSource basicDataSource = (BasicDataSource) Conexion.dataSource;
		basicDataSource.close();
		
		basicDataSource = new BasicDataSource();
		
		try {
			basicDataSource.setDriverClassName("org.postgresql.Driver");
			basicDataSource.setUsername(configuracion.getUsuarioservidor());
			basicDataSource.setPassword(configuracion.getPasswordservidor());
			basicDataSource.setUrl("jdbc:postgresql://" + 
					configuracion.getIpsevidor() + ":" + 
					configuracion.getPuertoservidor() + "/" + 
					configuracion.getBddsevidor());
			basicDataSource.setMaxActive(12);
			basicDataSource.setMaxIdle(12);
			basicDataSource.setInitialSize(3);
			basicDataSource.setMaxWait(10000);
			basicDataSource.setRemoveAbandonedTimeout(1500);
			
			Conexion.dataSource = basicDataSource;
		} catch (Exception ex) {log.warn(ex.getMessage());}
	}
}
