package com.atrums.modelo;

import org.apache.log4j.Logger;

public class DocumentoLinea {
	static final Logger log = Logger.getLogger(DocumentoLinea.class);
	private String itemcdgodtfc;
	private String dtfcdscr;
	private String dtfccntd;
	private String dtfcpcsimp;
	private String dtfcpcun;
	
	private String dtfctot;
	private String dtfcvaliva;
	private String totallinea;
	
	public DocumentoLinea() {
		super();
	}

	public String getDtfctot() {
		return dtfctot;
	}

	public void setDtfctot(String dtfctot) {
		this.dtfctot = dtfctot;
	}

	public String getDtfcvaliva() {
		return dtfcvaliva;
	}

	public void setDtfcvaliva(String dtfcvaliva) {
		this.dtfcvaliva = dtfcvaliva;
	}

	public String getTotallinea() {
		return totallinea;
	}

	public void setTotallinea(String totallinea) {
		this.totallinea = totallinea;
	}

	public String getDtfcpcun() {
		return dtfcpcun;
	}

	public void setDtfcpcun(String dtfcpcun) {
		this.dtfcpcun = dtfcpcun;
	}

	public String getDtfcpcsimp() {
		return dtfcpcsimp;
	}

	public void setDtfcpcsimp(String dtfcpcsimp) {
		this.dtfcpcsimp = dtfcpcsimp;
	}

	public String getDtfccntd() {
		return dtfccntd;
	}

	public void setDtfccntd(String dtfccntd) {
		this.dtfccntd = dtfccntd;
	}

	public String getDtfcdscr() {
		return dtfcdscr;
	}

	public void setDtfcdscr(String dtfcdscr) {
		this.dtfcdscr = dtfcdscr;
	}

	public String getItemcdgodtfc() {
		return itemcdgodtfc;
	}

	public void setItemcdgodtfc(String itemcdgodtfc) {
		this.itemcdgodtfc = itemcdgodtfc;
	}

}
