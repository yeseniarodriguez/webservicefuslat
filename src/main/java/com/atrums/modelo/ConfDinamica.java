package com.atrums.modelo;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class ConfDinamica {
	static final Logger log = Logger.getLogger(ConfDinamica.class);
    private static InputStream confService = null;
    private Properties propiedad = new Properties();
    private String consulta;
    private String clave;
    private String password;
    
    public ConfDinamica() {
    	super();
    }
    
    public ConfDinamica(String adclient) {
    	try {
			ConfDinamica.confService = this.getClass().getClassLoader().getResourceAsStream("./..//CONFIGURACION/configuracion.xml");
			this.propiedad.loadFromXML(confService);
			
			this.consulta = this.propiedad.getProperty("consulta" + adclient);
			this.clave = this.propiedad.getProperty("clave" + adclient);
			this.password = this.propiedad.getProperty("password" + adclient);
			
			this.propiedad.clear();
			ConfDinamica.confService.close();
		} catch (IOException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		}
    }

	public String getConsulta() {
		return consulta;
	}

	public void setConsulta(String consulta) {
		this.consulta = consulta;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
