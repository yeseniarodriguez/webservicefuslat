package com.atrums.modelo;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class Configuracion {
	static final Logger log = Logger.getLogger(Configuracion.class);
	private InputStream confService = null;
	Properties propiedad = new Properties();
	
	/**
	 * Postgres Server
	 * **/
	
	private String ipsevidor;
	private String puertoservidor;
	private String usuarioservidor;
	private String passwordservidor;
	private String bddsevidor;
	
	/**
	 * Empresa
	 * **/
	
	private String empresa;
	private String categoriaproducto;
	
	/**
	 * Correo
	 */
	
	private boolean enviarCorreo;
	private String asunto;
	private String cuerpoCorreo;
	
	
	public Configuracion() {
		try {
			this.confService = this.getClass().getClassLoader().getResourceAsStream("./..//CONFIGURACION/configuracion.xml");
			this.propiedad.loadFromXML(confService);
			
			/**
			 * Postgres Server
			 * **/
			
			this.ipsevidor = this.propiedad.getProperty("ipservidor");
			this.puertoservidor = this.propiedad.getProperty("puertoservidor");
			this.usuarioservidor = this.propiedad.getProperty("usuariosservidor");
			this.passwordservidor = this.propiedad.getProperty("passwordservidor");
			this.bddsevidor = this.propiedad.getProperty("bddservidor");
			
			/**
			 * Empresa
			 * **/
			
			this.empresa = this.propiedad.getProperty("empresa");
			this.categoriaproducto = this.propiedad.getProperty("categoriaproducto");
			
			/**
			 * Email
			 */
			
			this.enviarCorreo = this.propiedad.getProperty("EnviarCorreo").equals("Y")? true: false;
			this.asunto = this.propiedad.getProperty("Asunto");
			this.cuerpoCorreo = this.propiedad.getProperty("CuerpoCorreo");
			
			this.propiedad.clear();
			this.confService.close();
		} catch (IOException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		}
	}

	public boolean isEnviarCorreo() {
		return enviarCorreo;
	}

	public void setEnviarCorreo(boolean enviarCorreo) {
		this.enviarCorreo = enviarCorreo;
	}

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public String getCuerpoCorreo() {
		return cuerpoCorreo;
	}

	public void setCuerpoCorreo(String cuerpoCorreo) {
		this.cuerpoCorreo = cuerpoCorreo;
	}

	public String getCategoriaproducto() {
		return categoriaproducto;
	}

	public void setCategoriaproducto(String categoriaproducto) {
		this.categoriaproducto = categoriaproducto;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public String getIpsevidor() {
		return ipsevidor;
	}

	public void setIpsevidor(String ipsevidor) {
		this.ipsevidor = ipsevidor;
	}

	public String getPuertoservidor() {
		return puertoservidor;
	}

	public void setPuertoservidor(String puertoservidor) {
		this.puertoservidor = puertoservidor;
	}

	public String getUsuarioservidor() {
		return usuarioservidor;
	}

	public void setUsuarioservidor(String usuarioservidor) {
		this.usuarioservidor = usuarioservidor;
	}

	public String getPasswordservidor() {
		return passwordservidor;
	}

	public void setPasswordservidor(String passwordservidor) {
		this.passwordservidor = passwordservidor;
	}

	public String getBddsevidor() {
		return bddsevidor;
	}

	public void setBddsevidor(String bddsevidor) {
		this.bddsevidor = bddsevidor;
	}	
}
