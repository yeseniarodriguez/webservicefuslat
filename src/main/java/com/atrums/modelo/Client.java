package com.atrums.modelo;

import org.apache.log4j.Logger;

public class Client {
	static final Logger log = Logger.getLogger(Client.class);
	
	private String adClientId;
	private String name;
	private String direccion;
	private String direccionMatriz;
	private String ambiente;
	private String tipoEmision;
	private String codNumerico;
	private String numResolucion;
	private String razonSocial;
	private String nombreComercial;
	private boolean oblContabili;
	private String ruc;
	private String serverCorreo;
	
	public Client() {
		super();
	}

	public String getServerCorreo() {
		return serverCorreo;
	}

	public void setServerCorreo(String serverCorreo) {
		this.serverCorreo = serverCorreo;
	}

	public String getDireccionMatriz() {
		return direccionMatriz;
	}

	public void setDireccionMatriz(String direccionMatriz) {
		this.direccionMatriz = direccionMatriz;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getAmbiente() {
		return ambiente;
	}

	public void setAmbiente(String ambiente) {
		this.ambiente = ambiente;
	}

	public String getTipoEmision() {
		return tipoEmision;
	}

	public void setTipoEmision(String tipoEmision) {
		this.tipoEmision = tipoEmision;
	}

	public String getCodNumerico() {
		return codNumerico;
	}

	public void setCodNumerico(String codNumerico) {
		this.codNumerico = codNumerico;
	}

	public String getNumResolucion() {
		return numResolucion;
	}

	public void setNumResolucion(String numResolucion) {
		this.numResolucion = numResolucion;
	}

	public String getNombreComercial() {
		return nombreComercial;
	}

	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}

	public boolean isOblContabili() {
		return oblContabili;
	}

	public void setOblContabili(boolean oblContabili) {
		this.oblContabili = oblContabili;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getAdClientId() {
		return adClientId;
	}

	public void setAdClientId(String adClientId) {
		this.adClientId = adClientId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
