package com.atrums.modelo;

import java.util.ArrayList;
import java.util.List;

public class JsonFactura {
	private String nrodocumentoref;
	private String nrodocumentoopen;
	private String tipodoc;
	private String nombreempresa;
	private String nroestablecimiento;
	private String ptoemision;
	private String fechadocumento;
	private String claveacceso;
	private String cliente;
	private String nombrecliente;
	private String direccioncliente;
	private String emailcliente;
	private String totaldocumento;
	private String propina;
	
	private String fechaautoizacion;
	private String autorizacion;
	
	private List<Linea> lineas = new ArrayList<Linea>();
	private List<Pago> pagos = new ArrayList<Pago>();
	
	public JsonFactura() {
		super();
	}

	public String getFechaautoizacion() {
		return fechaautoizacion;
	}

	public void setFechaautoizacion(String fechaautoizacion) {
		this.fechaautoizacion = fechaautoizacion;
	}

	public String getAutorizacion() {
		return autorizacion;
	}

	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}

	public List<Pago> getPagos() {
		return pagos;
	}

	public void setPagos(List<Pago> pagos) {
		this.pagos = pagos;
	}

	public String getTotaldocumento() {
		return totaldocumento;
	}

	public void setTotaldocumento(String totaldocumento) {
		this.totaldocumento = totaldocumento;
	}

	public String getPropina() {
		return propina;
	}

	public void setPropina(String propina) {
		this.propina = propina;
	}

	public List<Linea> getLineas() {
		return lineas;
	}

	public void setLineas(List<Linea> lineas) {
		this.lineas = lineas;
	}

	public String getDireccioncliente() {
		return direccioncliente;
	}

	public void setDireccioncliente(String direccioncliente) {
		this.direccioncliente = direccioncliente;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public String getNombrecliente() {
		return nombrecliente;
	}

	public void setNombrecliente(String nombrecliente) {
		this.nombrecliente = nombrecliente;
	}

	public String getEmailcliente() {
		return emailcliente;
	}

	public void setEmailcliente(String emailcliente) {
		this.emailcliente = emailcliente;
	}

	public String getClaveacceso() {
		return claveacceso;
	}

	public void setClaveacceso(String claveacceso) {
		this.claveacceso = claveacceso;
	}

	public String getPtoemision() {
		return ptoemision;
	}

	public void setPtoemision(String ptoemision) {
		this.ptoemision = ptoemision;
	}

	public String getNroestablecimiento() {
		return nroestablecimiento;
	}

	public void setNroestablecimiento(String nroestablecimiento) {
		this.nroestablecimiento = nroestablecimiento;
	}

	public String getFechadocumento() {
		return fechadocumento;
	}

	public void setFechadocumento(String fechadocumento) {
		this.fechadocumento = fechadocumento;
	}
	
	public String getNrodocumentoref() {
		return nrodocumentoref;
	}

	public void setNrodocumentoref(String nrodocumentoref) {
		this.nrodocumentoref = nrodocumentoref;
	}

	public String getNrodocumentoopen() {
		return nrodocumentoopen;
	}

	public void setNrodocumentoopen(String nrodocumentoopen) {
		this.nrodocumentoopen = nrodocumentoopen;
	}

	public String getNombreempresa() {
		return nombreempresa;
	}

	public void setNombreempresa(String nombreempresa) {
		this.nombreempresa = nombreempresa;
	}

	public String getTipodoc() {
		return tipodoc;
	}

	public void setTipodoc(String tipodoc) {
		this.tipodoc = tipodoc;
	}
}
