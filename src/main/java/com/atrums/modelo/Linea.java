package com.atrums.modelo;

public class Linea {
	private String precio;	
	private String descuento;
	private String cantidad;
	private String porcentajeimpuesto;
	private String codigoimpuesto;
	private String totallinea;
	private String impuestolinea;
	private String tipoimpuesto;
	private String codigoProducto;
	private String nombreProducto;
	
	public Linea() {
		super();
	}

	public String getCodigoimpuesto() {
		return codigoimpuesto;
	}

	public void setCodigoimpuesto(String codigoimpuesto) {
		this.codigoimpuesto = codigoimpuesto;
	}

	public String getCodigoProducto() {
		return codigoProducto;
	}

	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}

	public String getNombreProducto() {
		return nombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

	public String getTipoimpuesto() {
		return tipoimpuesto;
	}

	public void setTipoimpuesto(String tipoimpuesto) {
		this.tipoimpuesto = tipoimpuesto;
	}

	public String getImpuestolinea() {
		return impuestolinea;
	}

	public void setImpuestolinea(String impuestolinea) {
		this.impuestolinea = impuestolinea;
	}

	public String getTotallinea() {
		return totallinea;
	}

	public void setTotallinea(String totallinea) {
		this.totallinea = totallinea;
	}

	public String getPorcentajeimpuesto() {
		return porcentajeimpuesto;
	}

	public void setPorcentajeimpuesto(String porcentajeimpuesto) {
		this.porcentajeimpuesto = porcentajeimpuesto;
	}

	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	public String getPrecio() {
		return precio;
	}

	public void setPrecio(String precio) {
		this.precio = precio;
	}

	public String getDescuento() {
		return descuento;
	}

	public void setDescuento(String descuento) {
		this.descuento = descuento;
	}
}
