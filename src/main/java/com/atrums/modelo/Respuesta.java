package com.atrums.modelo;

import org.apache.log4j.Logger;

import com.atrums.dao.Operaciones;

public class Respuesta {
	static final Logger log = Logger.getLogger(Operaciones.class);
	
	private String empresa;
	private String nrodocumento;
	private String sucursal;
	private String ptoemision;
	private String tipodocumento;
	private String fecha;
	private String claveacceso;
	private String ruccliente;
	private String fechaautorizacion;
	private String autorizacion;
	private String mensaje;
	private String mensajeerror;
	private boolean procesado;
	
	public Respuesta() {
		super();
	}

	public String getFechaautorizacion() {
		return fechaautorizacion;
	}

	public void setFechaautorizacion(String fechaautorizacion) {
		this.fechaautorizacion = fechaautorizacion;
	}

	public String getAutorizacion() {
		return autorizacion;
	}

	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}

	public boolean isProcesado() {
		return procesado;
	}

	public void setProcesado(boolean procesado) {
		this.procesado = procesado;
	}

	public String getRuccliente() {
		return ruccliente;
	}

	public void setRuccliente(String ruccliente) {
		this.ruccliente = ruccliente;
	}

	public String getSucursal() {
		return sucursal;
	}

	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}

	public String getPtoemision() {
		return ptoemision;
	}

	public void setPtoemision(String ptoemision) {
		this.ptoemision = ptoemision;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public String getNrodocumento() {
		return nrodocumento;
	}

	public void setNrodocumento(String nrodocumento) {
		this.nrodocumento = nrodocumento;
	}

	public String getTipodocumento() {
		return tipodocumento;
	}

	public void setTipodocumento(String tipodocumento) {
		this.tipodocumento = tipodocumento;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getClaveacceso() {
		return claveacceso;
	}

	public void setClaveacceso(String claveacceso) {
		this.claveacceso = claveacceso;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getMensajeerror() {
		return mensajeerror;
	}

	public void setMensajeerror(String mensajeerror) {
		this.mensajeerror = mensajeerror;
	}
	
	public void setJsonFactura(JsonFactura jsonFactura) {
		if (jsonFactura.getNrodocumentoopen() != null) {
			this.nrodocumento = jsonFactura.getNrodocumentoopen();
		} else {
			this.nrodocumento = jsonFactura.getNrodocumentoref();
		}
		
		this.tipodocumento = jsonFactura.getTipodoc();
		this.empresa = jsonFactura.getNombreempresa();
		this.claveacceso = jsonFactura.getClaveacceso();
		this.sucursal = jsonFactura.getNroestablecimiento();
		this.ptoemision = jsonFactura.getPtoemision();
		this.fecha = jsonFactura.getFechadocumento();
		this.ruccliente = jsonFactura.getCliente();
		this.fechaautorizacion = jsonFactura.getFechaautoizacion();
		this.autorizacion = jsonFactura.getAutorizacion();
	}

	public void setJsonNotaCredito(JsonNotaCredito jsonNotaCredito) {
		if (jsonNotaCredito.getNrodocumentoopen() != null) {
			this.nrodocumento = jsonNotaCredito.getNrodocumentoopen();
		} else {
			this.nrodocumento = jsonNotaCredito.getNrodocumentoref();
		}
		
		this.nrodocumento = jsonNotaCredito.getNrodocumentoopen();
		this.tipodocumento = jsonNotaCredito.getTipodoc();
		this.empresa = jsonNotaCredito.getNombreempresa();
		this.claveacceso = jsonNotaCredito.getClaveacceso();
		this.sucursal = jsonNotaCredito.getNroestablecimiento();
		this.ptoemision = jsonNotaCredito.getPtoemision();
		this.fecha = jsonNotaCredito.getFechadocumento();
		this.ruccliente = jsonNotaCredito.getCliente();
		this.fechaautorizacion = jsonNotaCredito.getFechaautoizacion();
		this.autorizacion = jsonNotaCredito.getAutorizacion();
	}
	
}
