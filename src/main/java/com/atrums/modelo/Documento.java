package com.atrums.modelo;

import org.apache.log4j.Logger;

public class Documento {
	static final Logger log = Logger.getLogger(Documento.class);
	
	private String fctrcdgo;
	private String ban_erp;
	private String fctrfcha;
	private String fctrnmro;
	private String etdocdgo;
	private String clntcdgofctr;
	private String fctrprsn;
	private String fctrdire;
	private String fctrclntmail;
	private String fctrpgdo;
	private String cliid;
	
	public Documento() {
		super();
	}

	public String getCliid() {
		return cliid;
	}

	public void setCliid(String cliid) {
		this.cliid = cliid;
	}

	public String getFctrpgdo() {
		return fctrpgdo;
	}

	public void setFctrpgdo(String fctrpgdo) {
		this.fctrpgdo = fctrpgdo;
	}

	public String getFctrprsn() {
		return fctrprsn;
	}

	public void setFctrprsn(String fctrprsn) {
		this.fctrprsn = fctrprsn;
	}

	public String getFctrdire() {
		return fctrdire;
	}

	public void setFctrdire(String fctrdire) {
		this.fctrdire = fctrdire;
	}

	public String getFctrclntmail() {
		return fctrclntmail;
	}

	public void setFctrclntmail(String fctrclntmail) {
		this.fctrclntmail = fctrclntmail;
	}

	public String getClntcdgofctr() {
		return clntcdgofctr;
	}

	public void setClntcdgofctr(String clntcdgofctr) {
		this.clntcdgofctr = clntcdgofctr;
	}

	public String getFctrcdgo() {
		return fctrcdgo;
	}

	public void setFctrcdgo(String fctrcdgo) {
		this.fctrcdgo = fctrcdgo;
	}

	public String getBan_erp() {
		return ban_erp;
	}

	public void setBan_erp(String ban_erp) {
		this.ban_erp = ban_erp;
	}

	public String getFctrfcha() {
		return fctrfcha;
	}

	public void setFctrfcha(String fctrfcha) {
		this.fctrfcha = fctrfcha;
	}

	public String getFctrnmro() {
		return fctrnmro;
	}

	public void setFctrnmro(String fctrnmro) {
		this.fctrnmro = fctrnmro;
	}

	public String getEtdocdgo() {
		return etdocdgo;
	}

	public void setEtdocdgo(String etdocdgo) {
		this.etdocdgo = etdocdgo;
	}
}
