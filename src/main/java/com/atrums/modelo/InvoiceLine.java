package com.atrums.modelo;

import org.apache.log4j.Logger;

public class InvoiceLine {
	static final Logger log = Logger.getLogger(InvoiceLine.class);
	
	private String mProductId;
	private int linea;
	private String cantidad;
	private String precioUnitario;
	private String totalLineaSinImpuestos;
	private String totalLinea;
	private String descuento;
	private String servicio;
	private String porcentaje;
	private String valor;
	private String cInvoiceLineId;
	private String cTaxId;
	private String porcentajeDescuento;
	private String mProductCategoryId;
	
	public InvoiceLine() {
		super();
	}

	public String getmProductCategoryId() {
		return mProductCategoryId;
	}

	public void setmProductCategoryId(String mProductCategoryId) {
		this.mProductCategoryId = mProductCategoryId;
	}

	public String getServicio() {
		return servicio;
	}

	public void setServicio(String servicio) {
		this.servicio = servicio;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getTotalLineaSinImpuestos() {
		return totalLineaSinImpuestos;
	}

	public void setTotalLineaSinImpuestos(String totalLineaSinImpuestos) {
		this.totalLineaSinImpuestos = totalLineaSinImpuestos;
	}

	public String getTotalLinea() {
		return totalLinea;
	}

	public void setTotalLinea(String totalLinea) {
		this.totalLinea = totalLinea;
	}

	public String getPorcentajeDescuento() {
		return porcentajeDescuento;
	}

	public void setPorcentajeDescuento(String porcentajeDescuento) {
		this.porcentajeDescuento = porcentajeDescuento;
	}

	public String getDescuento() {
		return descuento;
	}

	public void setDescuento(String descuento) {
		this.descuento = descuento;
	}

	public String getmProductId() {
		return mProductId;
	}

	public void setmProductId(String mProductId) {
		this.mProductId = mProductId;
	}

	public String getcInvoiceLineId() {
		return cInvoiceLineId;
	}

	public void setcInvoiceLineId(String cInvoiceLineId) {
		this.cInvoiceLineId = cInvoiceLineId;
	}

	public String getcTaxId() {
		return cTaxId;
	}

	public void setcTaxId(String cTaxId) {
		this.cTaxId = cTaxId;
	}

	public String getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(String porcentaje) {
		this.porcentaje = porcentaje;
	}

	public String getPrecioUnitario() {
		return precioUnitario;
	}

	public void setPrecioUnitario(String precioUnitario) {
		this.precioUnitario = precioUnitario;
	}

	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	public int getLinea() {
		return linea;
	}

	public void setLinea(int linea) {
		this.linea = linea;
	}
}
