package com.atrums.modelo;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class ConfService {
	static final Logger log = Logger.getLogger(ConfService.class);
    private static InputStream confService = null;
    private Properties propiedad = new Properties();
    
    private String pruebasRt;
    private String pruebasAt;
    private String produccionRt;
    private String produccionAt;
    private String nameSpaceRt;
    private String nameSpaceAt;
        
    public ConfService(){
        try {
            confService = this.getClass().getClassLoader().getResourceAsStream("./..//CONFIGURACION/configuracion.xml");
            propiedad.loadFromXML(confService);
            
        	this.pruebasRt = propiedad.getProperty("PruebasRt");
        	this.pruebasAt = propiedad.getProperty("PruebasAt");
        	this.produccionRt = propiedad.getProperty("ProduccionRt");
        	this.produccionAt = propiedad.getProperty("ProduccionAt");
        	this.nameSpaceRt = propiedad.getProperty("NameSpaceRt");
        	this.nameSpaceAt = propiedad.getProperty("NameSpaceAt");
        	
        	confService.close();
        } catch (IOException ex) {
            log.error(ex.getStackTrace());
        }
    }

	public static InputStream getConfService() {
		return confService;
	}

	public static void setConfService(InputStream confService) {
		ConfService.confService = confService;
	}

	public String getPruebasRt() {
		return pruebasRt;
	}

	public void setPruebasRt(String pruebasRt) {
		this.pruebasRt = pruebasRt;
	}

	public String getPruebasAt() {
		return pruebasAt;
	}

	public void setPruebasAt(String pruebasAt) {
		this.pruebasAt = pruebasAt;
	}

	public String getProduccionRt() {
		return produccionRt;
	}

	public void setProduccionRt(String produccionRt) {
		this.produccionRt = produccionRt;
	}

	public String getProduccionAt() {
		return produccionAt;
	}

	public void setProduccionAt(String produccionAt) {
		this.produccionAt = produccionAt;
	}

	public String getNameSpaceRt() {
		return nameSpaceRt;
	}

	public void setNameSpaceRt(String nameSpaceRt) {
		this.nameSpaceRt = nameSpaceRt;
	}

	public String getNameSpaceAt() {
		return nameSpaceAt;
	}

	public void setNameSpaceAt(String nameSpaceAt) {
		this.nameSpaceAt = nameSpaceAt;
	}
}
