package com.atrums.modelo;

import org.apache.log4j.Logger;

public class PocEmail {
	static final Logger log = Logger.getLogger(Pago.class);
	
	private String smtpserver = null;
	private String smtpserveracount = null;
	private String smtppassword = null;
	private boolean smtpautorization = false;
	private String smtpsegurity = null;
	private String smtpport = null;
	
	public PocEmail() {
		super();
	}

	public boolean isSmtpautorization() {
		return smtpautorization;
	}

	public void setSmtpautorization(boolean smtpautorization) {
		this.smtpautorization = smtpautorization;
	}

	public String getSmtpserver() {
		return smtpserver;
	}

	public void setSmtpserver(String smtpserver) {
		this.smtpserver = smtpserver;
	}

	public String getSmtpserveracount() {
		return smtpserveracount;
	}

	public void setSmtpserveracount(String smtpserveracount) {
		this.smtpserveracount = smtpserveracount;
	}

	public String getSmtppassword() {
		return smtppassword;
	}

	public void setSmtppassword(String smtppassword) {
		this.smtppassword = smtppassword;
	}

	public String getSmtpsegurity() {
		return smtpsegurity;
	}

	public void setSmtpsegurity(String smtpsegurity) {
		this.smtpsegurity = smtpsegurity;
	}

	public String getSmtpport() {
		return smtpport;
	}

	public void setSmtpport(String smtpport) {
		this.smtpport = smtpport;
	}
}
