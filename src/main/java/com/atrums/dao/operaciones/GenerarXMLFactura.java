package com.atrums.dao.operaciones;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import com.atrums.modelo.Client;
import com.atrums.modelo.ConfDinamica;
import com.atrums.modelo.Impuesto;
import com.atrums.modelo.JsonFactura;
import com.atrums.modelo.Linea;
import com.atrums.modelo.Pago;
//import com.atrums.persistencia.OperacionesBDDPostgres;

public class GenerarXMLFactura {
	static final Logger log = Logger.getLogger(GenerarXMLFactura.class);
	private String mensaje = null;
	private Client infoTributaria = null;
	private JsonFactura jsonFactura = null;
	//private Connection connection = null;
	private String claveacceso = null;
	private DecimalFormat format2 = new DecimalFormat("0.00");
	private OperacionesAuxiliares auxiliares =  new OperacionesAuxiliares();
	//private OperacionesBDDPostgres bddPostgres = new OperacionesBDDPostgres(); 
	
	public GenerarXMLFactura(JsonFactura jsonFactura, Connection connection) {
		this.jsonFactura = jsonFactura;
		//this.connection = connection;
	}

	public String getClaveacceso() {
		return claveacceso;
	}

	public void setClaveacceso(String claveacceso) {
		this.claveacceso = claveacceso;
	}

	public Client getInfoTributaria() {
		return infoTributaria;
	}

	public void setInfoTributaria(Client infoTributaria) {
		this.infoTributaria = infoTributaria;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String generarXML() {
		log.info("Generando documento: " + this.jsonFactura.getNrodocumentoopen());
		File file = null;
		String fileString = null;
		double auxTotal = 0;
		double auxTotalVerificacion = 0;
		double subtotal = 0;
		ConfDinamica dinamica = new ConfDinamica(this.infoTributaria.getRuc());
		
		try {
			file = File.createTempFile("documento", ".xml", null);
			Document document = DocumentHelper.createDocument();
			OutputFormat outputFormat = OutputFormat.createPrettyPrint();
			
			Element elmfac = null;
			
			String tipoComprobante = "";
			
			elmfac = document.addElement("factura");
			elmfac.addAttribute("id", "comprobante");
			elmfac.addAttribute("version", "1.1.0");
			
			if (this.jsonFactura.getTipodoc().length() > 2 || 
					this.jsonFactura.getTipodoc().length() == 0) {
				this.setMensaje("El tipo de documento enviado no es una factura");
			} else if (this.jsonFactura.getTipodoc().length() < 2) {
				tipoComprobante = "0" + this.jsonFactura.getTipodoc();
			} else {
				tipoComprobante = this.jsonFactura.getTipodoc();
			}
			
			final Element elminftri = elmfac.addElement("infoTributaria");
			
			elminftri.addElement("ambiente").addText(this.infoTributaria.getAmbiente());
			elminftri.addElement("tipoEmision").addText(this.infoTributaria.getTipoEmision());
			elminftri.addElement("razonSocial").addText(this.auxiliares.normalizacionPalabras(this.infoTributaria.getRazonSocial()));
			elminftri.addElement("nombreComercial").addText(this.auxiliares.normalizacionPalabras(this.infoTributaria.getNombreComercial()));
			elminftri.addElement("ruc").addText(this.infoTributaria.getRuc());
			
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
			Date fechaDoc = format.parse(jsonFactura.getFechadocumento());
			SimpleDateFormat sdfFormato = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat sdfFormatoClave = new SimpleDateFormat("ddMMyyyy");
			
			String secuencial = this.jsonFactura.getNrodocumentoopen();
			for (int i = 0; i < (9 - this.jsonFactura.getNrodocumentoopen().length()); i++) {
				secuencial = "0" + secuencial;
			}
			
			String codNumerico = this.infoTributaria.getCodNumerico();
			
			if(this.infoTributaria.getCodNumerico() != null){
				for (int i = 0; i < (8 - this.infoTributaria.getCodNumerico().length()); i++) {
					codNumerico = "0" + codNumerico;
				}
			}
			
			this.claveacceso = this.auxiliares.generarclaveacceso(
					sdfFormatoClave.format(fechaDoc), 
					tipoComprobante, 
					this.infoTributaria.getRuc(), 
					this.infoTributaria.getAmbiente(), 
					this.jsonFactura.getNroestablecimiento() + this.jsonFactura.getPtoemision(), 
					secuencial, 
					codNumerico, 
					this.infoTributaria.getTipoEmision());
			
			/*if (!this.bddPostgres.existeClaveAcceso(this.claveacceso, connection)) {
				this.mensaje = "ERROR / Ya existe un documento con el mismo nro de documento, establecimiento, sucursal, fecha y tipo de documento autorizado.";
				return null;
			}*/
			
			fileString = null;
			
			this.jsonFactura.setClaveacceso(this.claveacceso);
			
			elminftri.addElement("claveAcceso").addText(this.claveacceso);
			elminftri.addElement("codDoc").addText(tipoComprobante);
			elminftri.addElement("estab").addText(this.jsonFactura.getNroestablecimiento());
			elminftri.addElement("ptoEmi").addText(this.jsonFactura.getPtoemision());
			elminftri.addElement("secuencial").addText(secuencial);
			elminftri.addElement("dirMatriz").addText(this.auxiliares.normalizacionPalabras(this.infoTributaria.getDireccionMatriz()));
			
			Element elminffac = null;
			elminffac = elmfac.addElement("infoFactura");
			
			elminffac.addElement("fechaEmision").addText(sdfFormato.format(fechaDoc));
			elminffac.addElement("dirEstablecimiento").addText(this.auxiliares.normalizacionPalabras(this.infoTributaria.getDireccion()));
			
			if(this.infoTributaria.getNumResolucion() != null){
				String numResolucion = this.infoTributaria.getNumResolucion();
				
				for (int i = 0; i < (3 - this.infoTributaria.getNumResolucion().length()); i++) {
					numResolucion = "0" + numResolucion;
				}
				
				elminffac.addElement("contribuyenteEspecial").addText(numResolucion);
			}
			
			if(this.infoTributaria.isOblContabili()){
				elminffac.addElement("obligadoContabilidad").addText("SI");
			}else{
				elminffac.addElement("obligadoContabilidad").addText("NO");
			}
			
			if(this.jsonFactura.getCliente().equals("9999999999999")){
				elminffac.addElement("tipoIdentificacionComprador").addText("07");
			}else if(this.jsonFactura.getCliente().length() == 13 && 
					this.jsonFactura.getCliente().substring(10, this.jsonFactura.getCliente().length()).equals("001")){
				elminffac.addElement("tipoIdentificacionComprador").addText("04");
			}else if(this.jsonFactura.getCliente().length() == 10 && 
					auxiliares.isNumeric(this.jsonFactura.getCliente())){
				elminffac.addElement("tipoIdentificacionComprador").addText("05");
			}else{
				elminffac.addElement("tipoIdentificacionComprador").addText("06");
			}
			
			if(this.jsonFactura.getCliente().equals("9999999999999")){
				elminffac.addElement("razonSocialComprador").addText("CONSUMIDOR FINAL");
			}else{
				elminffac.addElement("razonSocialComprador").addText(this.jsonFactura.getNombrecliente());
			}
			
			elminffac.addElement("identificacionComprador").addText(this.jsonFactura.getCliente());
			
			Element totalSinImpuestos = elminffac.addElement("totalSinImpuestos");
			
			Element elmDescTot = null;
			
			elmDescTot = elminffac.addElement("totalDescuento");
			
			Element elmtolcimp = null;
			elmtolcimp = elminffac.addElement("totalConImpuestos");
			
			List<Impuesto> impuestos = new ArrayList<Impuesto>();
			
			List<Linea> lineas = this.jsonFactura.getLineas();
			
			for(int i=0;i<lineas.size();i++){
				boolean encontrado = true;
				
				double auxTotalLinea = Double.valueOf(lineas.get(i).getTotallinea());
				double auxValorImpues = Double.valueOf(lineas.get(i).getImpuestolinea());
				
				auxTotalVerificacion = auxTotalVerificacion + auxTotalLinea + auxValorImpues;
				
				for(int j=0;j<impuestos.size();j++){
					String auxPorcentaje = "";
					
					if(Integer.valueOf(Double.valueOf(lineas.get(i).getPorcentajeimpuesto()).intValue()) == 0){
						auxPorcentaje = "0";
					}else if(Integer.valueOf(Double.valueOf(lineas.get(i).getPorcentajeimpuesto()).intValue()) == 12){
						auxPorcentaje = "2";
					}else if(Integer.valueOf(Double.valueOf(lineas.get(i).getPorcentajeimpuesto()).intValue()) == 14){
						auxPorcentaje = "3";
					}
					
					String auxTipoImpuesto = "";
					
					if (lineas.get(i).getTipoimpuesto().equals("IVA")) {
						auxTipoImpuesto = "2";
					} else if (lineas.get(i).getTipoimpuesto().equals("ICE")) {
						auxTipoImpuesto = "3";
					} else if (lineas.get(i).getTipoimpuesto().equals("IRBPNR")) {
						auxTipoImpuesto = "5";
					}
					
					if(impuestos.get(j).getCodigoImpuesto().equals(auxTipoImpuesto) && 
							impuestos.get(j).getPorcentajeImpuesto().equals(auxPorcentaje)){
						encontrado = false;
						
						impuestos.get(j).setBaseImponible(impuestos.get(j).getBaseImponible() + 
								Double.valueOf(format2.format(auxTotalLinea).replace(",", ".")));
						impuestos.get(j).setValor(impuestos.get(j).getValor() + 
								Double.valueOf(format2.format(auxValorImpues).replace(",", ".")));
					}
				}
				
				if(encontrado){
					Impuesto auxImp = new Impuesto();
					
					if (lineas.get(i).getTipoimpuesto().equals("IVA")) {
						auxImp.setCodigoImpuesto("2");
						
						if(Integer.valueOf(Double.valueOf(lineas.get(i).getPorcentajeimpuesto()).intValue()) == 0){
							auxImp.setPorcentajeImpuesto("0");
						}else if(Integer.valueOf(Double.valueOf(lineas.get(i).getPorcentajeimpuesto()).intValue()) == 12){
							auxImp.setPorcentajeImpuesto("2");
						}else if(Integer.valueOf(Double.valueOf(lineas.get(i).getPorcentajeimpuesto()).intValue()) == 14){
							auxImp.setPorcentajeImpuesto("3");
						} else {
							this.mensaje = "No existe el " + lineas.get(i).getPorcentajeimpuesto() + " en el sistema tributario del SRI.";
							return null;
						}
						
						auxImp.setBaseImponible(auxImp.getBaseImponible() + 
								Double.valueOf(format2.format(auxTotalLinea).replace(",", ".")));
						auxImp.setValor(auxImp.getValor() + 
								Double.valueOf(format2.format(auxValorImpues).replace(",", ".")));
						
						impuestos.add(auxImp);
					} else if (lineas.get(i).getTipoimpuesto().equals("ICE")) {
						auxImp.setCodigoImpuesto("3");
						auxImp.setPorcentajeImpuesto(lineas.get(i).getCodigoimpuesto());
						
						auxImp.setBaseImponible(auxImp.getBaseImponible() + 
								Double.valueOf(format2.format(auxTotalLinea).replace(",", ".")));
						auxImp.setValor(auxImp.getValor() + 
								Double.valueOf(format2.format(auxValorImpues).replace(",", ".")));
						
						impuestos.add(auxImp);
					} else if (lineas.get(i).getTipoimpuesto().equals("IRBPNR")) {
						auxImp.setCodigoImpuesto("5");
						auxImp.setPorcentajeImpuesto(lineas.get(i).getCodigoimpuesto());
						
						auxImp.setBaseImponible(auxImp.getBaseImponible() + 
								Double.valueOf(format2.format(auxTotalLinea).replace(",", ".")));
						auxImp.setValor(auxImp.getValor() + 
								Double.valueOf(format2.format(auxValorImpues).replace(",", ".")));
						
						impuestos.add(auxImp);
					}
				}
			}
			
			for(int k=0;k<impuestos.size();k++){
				Element elmtolimp = null;
				elmtolimp = elmtolcimp.addElement("totalImpuesto");
				
				elmtolimp.addElement("codigo").addText(impuestos.get(k).getCodigoImpuesto());
				elmtolimp.addElement("codigoPorcentaje").addText(impuestos.get(k).getPorcentajeImpuesto());
				
				elmtolimp.addElement("baseImponible").addText(format2.format(impuestos.get(k).getBaseImponible()).replace(",", "."));
	            elmtolimp.addElement("valor").addText(format2.format(impuestos.get(k).getValor()).replace(",", "."));
			}
			
			elminffac.addElement("propina").addText(format2.format(Double.valueOf(this.jsonFactura.getPropina())).replace(",", "."));
			elminffac.addElement("importeTotal").addText(format2.format(Double.valueOf(this.jsonFactura.getTotaldocumento())).replace(",", "."));
			auxTotal = Double.valueOf(format2.format(Double.valueOf(this.jsonFactura.getTotaldocumento())).replace(",", "."));
			auxTotalVerificacion = Double.valueOf(format2.format(auxTotalVerificacion).replace(",", "."));
			
			if (auxTotal != auxTotalVerificacion) {
				
				/*&& 
				auxTotal != (auxTotalVerificacion - 0.01) && 
				auxTotal != (auxTotalVerificacion + 0.01) && 
				auxTotal != (auxTotalVerificacion - 0.02) && 
				auxTotal != (auxTotalVerificacion + 0.02)*/
				
				this.mensaje = "El total del documento no concuerda con la suma de las lineas, total documento: " + auxTotal + ", total lineas: " + auxTotalVerificacion;
				return null;
			}
			
			elminffac.addElement("moneda").addText("DOLAR");
			
			if(!this.jsonFactura.getPagos().isEmpty()){
				Element elmpagos = null;
				elmpagos = elminffac.addElement("pagos");
				
				for(Pago auxpago: this.jsonFactura.getPagos()){
					Element elmpago = null;
					
					elmpago = elmpagos.addElement("pago");
					
					if (auxpago.getMetodoPago().equals("SIN UTILIZACION DEL SISTEMA FINANCIERO")) {
						elmpago.addElement("formaPago").addText("01");
					} else if (auxpago.getMetodoPago().equals("COMPENSACI�N DE DEUDAS") || 
							auxpago.getMetodoPago().equals("COMPENSACION DE DEUDAS")) {
						elmpago.addElement("formaPago").addText("15");
					} else if (auxpago.getMetodoPago().equals("TARJETA DE D�BITO") || 
							auxpago.getMetodoPago().equals("TARJETA DE DEBITO")) {
						elmpago.addElement("formaPago").addText("16");
					} else if (auxpago.getMetodoPago().equals("DINERO ELECTR�NICO") || 
							auxpago.getMetodoPago().equals("DINERO ELECTRONICO")) {
						elmpago.addElement("formaPago").addText("17");
					} else if (auxpago.getMetodoPago().equals("TARJETA PREPAGO")) {
						elmpago.addElement("formaPago").addText("18");
					} else if (auxpago.getMetodoPago().equals("TARJETA DE CR�DITO") || 
							auxpago.getMetodoPago().equals("TARJETA DE CREDITO")) {
						elmpago.addElement("formaPago").addText("19");
					} else if (auxpago.getMetodoPago().equals("OTROS CON UTILIZACI�N DEL SISTEMA FINANCIERO") || 
							auxpago.getMetodoPago().equals("OTROS CON UTILIZACION DEL SISTEMA FINANCIERO")) {
						elmpago.addElement("formaPago").addText("20");
					} else if (auxpago.getMetodoPago().equals("ENDOSO DE T�TULOS") || 
							auxpago.getMetodoPago().equals("ENDOSO DE TITULOS")) {
						elmpago.addElement("formaPago").addText("21");
					} else {
						this.mensaje = "No existe el m�todo de pago que se declaro en los pagos.";
						return null;
					}
					
					elmpago.addElement("total").addText(format2.format(Double.valueOf(auxpago.getTotalPago())).replace(",", "."));
					elmpago.addElement("plazo").addText("0");
					elmpago.addElement("unidadTiempo").addText("dias");
				}
			}
			
			Element elmdetfac = null;
			elmdetfac = elmfac.addElement("detalles");
			
			double descuento = 0;
			
			for(int i=0;i<lineas.size();i++){
				Element elmdeta = null;
				elmdeta = elmdetfac.addElement("detalle");
				
				String codProducto = ""; 
				if(lineas.get(i).getCodigoProducto().replace(" ", "").toLowerCase().length() > 25){
					codProducto = auxiliares.limpiarCodigoProducto(lineas.get(i).getCodigoProducto().substring(0, 25));
				}else{
					codProducto = auxiliares.limpiarCodigoProducto(lineas.get(i).getCodigoProducto());
				}
				
				elmdeta.addElement("codigoPrincipal").addText(codProducto);
				
				elmdeta.addElement("codigoAuxiliar").addText(codProducto);
				
				elmdeta.addElement("descripcion").addText(auxiliares.limpiarNombre(lineas.get(i).getNombreProducto()));
				elmdeta.addElement("cantidad").addText(lineas.get(i).getCantidad());
				elmdeta.addElement("precioUnitario").addText(format2.format(Double.valueOf(lineas.get(i).getPrecio())).replace(",", "."));
	            elmdeta.addElement("descuento").addText(format2.format(Double.valueOf(lineas.get(i).getDescuento())).replace(",", "."));
	            
	            descuento = descuento + Double.valueOf(lineas.get(i).getDescuento());
	            subtotal = subtotal +  Double.valueOf(lineas.get(i).getTotallinea());
	            elmdeta.addElement("precioTotalSinImpuesto").addText(format2.format(Double.valueOf(lineas.get(i).getTotallinea())).replace(",", "."));
	            
	            Element elmdetimps = null;
	            elmdetimps = elmdeta.addElement("impuestos");
	            Element elmdetimp = elmdetimps.addElement("impuesto");
	            
	            if (lineas.get(i).getTipoimpuesto().equals("IVA")) {
	            	elmdetimp.addElement("codigo").addText("2");
	            	
	            	if(Integer.valueOf(Double.valueOf(lineas.get(i).getPorcentajeimpuesto()).intValue()) == 0){
		            	elmdetimp.addElement("codigoPorcentaje").addText("0");
					}else if(Integer.valueOf(Double.valueOf(lineas.get(i).getPorcentajeimpuesto()).intValue()) == 12){
						elmdetimp.addElement("codigoPorcentaje").addText("2");
					}else if(Integer.valueOf(Double.valueOf(lineas.get(i).getPorcentajeimpuesto()).intValue()) == 14){
						elmdetimp.addElement("codigoPorcentaje").addText("3");
					} else {
						elmdetimp.addElement("codigoPorcentaje").addText(lineas.get(i).getCodigoimpuesto());
					}
	            	
	            	elmdetimp.addElement("tarifa").addText(format2.format(Double.valueOf(lineas.get(i).getPorcentajeimpuesto())).replace(",", "."));
		            elmdetimp.addElement("baseImponible").addText(format2.format(Double.valueOf(lineas.get(i).getTotallinea())).replace(",", "."));
		            
		            double auxValorImpues = Double.valueOf(lineas.get(i).getImpuestolinea());
		            
		            elmdetimp.addElement("valor").addText(format2.format(auxValorImpues).replace(",", "."));
				} else if (lineas.get(i).getTipoimpuesto().equals("ICE")) {
					elmdetimp.addElement("codigo").addText("3");
					elmdetimp.addElement("codigoPorcentaje").addText(lineas.get(i).getCodigoimpuesto());
					
					elmdetimp.addElement("tarifa").addText(format2.format(Double.valueOf(lineas.get(i).getPorcentajeimpuesto())).replace(",", "."));
		            elmdetimp.addElement("baseImponible").addText(format2.format(Double.valueOf(lineas.get(i).getTotallinea())).replace(",", "."));
		            
		            double auxValorImpues = Double.valueOf(lineas.get(i).getImpuestolinea());
		            
		            elmdetimp.addElement("valor").addText(format2.format(auxValorImpues).replace(",", "."));
				} else if (lineas.get(i).getTipoimpuesto().equals("IRBPNR")) {
					elmdetimp.addElement("codigo").addText("5");
					elmdetimp.addElement("codigoPorcentaje").addText(lineas.get(i).getCodigoimpuesto());
					
					elmdetimp.addElement("tarifa").addText(format2.format(Double.valueOf(lineas.get(i).getPorcentajeimpuesto())).replace(",", "."));
		            elmdetimp.addElement("baseImponible").addText(format2.format(Double.valueOf(lineas.get(i).getTotallinea())).replace(",", "."));
		            
		            double auxValorImpues = Double.valueOf(lineas.get(i).getImpuestolinea());
		            
		            elmdetimp.addElement("valor").addText(format2.format(auxValorImpues).replace(",", "."));
				}
			}
			
			totalSinImpuestos.addText(format2.format(subtotal).replace(",", "."));
			elmDescTot.setText(format2.format(descuento).replace(",", "."));
			
			if(this.infoTributaria.getServerCorreo() != null || dinamica.getConsulta() != null){
				Element elminfAdc = elmfac.addElement("infoAdicional");
				
				if(this.infoTributaria.getServerCorreo() != null){
					elminfAdc.addElement("campoAdicional").addAttribute("nombre", "EMAiL").addText(this.infoTributaria.getServerCorreo());
				}
				
				if(dinamica. getConsulta() != null){
					elminfAdc.addElement("campoAdicional").addAttribute("nombre", "PORTAL-CONSULTA").addText(dinamica.getConsulta());
				}
			}
			
			final XMLWriter writer = new XMLWriter(new OutputStreamWriter(new FileOutputStream(file),"utf-8"), outputFormat);
			writer.write(document);
			writer.flush();
			writer.close();
			
			if (dinamica.getClave() != null) {
				File fileLlave = new File(dinamica.getClave());
				
				if(fileLlave.exists()){
					//file = auxiliares.firmarDocumento(file, dinamica);
					
					if(file != null){
						byte[] bytes = auxiliares.filetobyte(file);
						fileString = new String(bytes, "UTF-8");
					}
				}else{
					fileString = null;
					this.mensaje = "ERROR / No hay la llave publica para firmar el documento";
				}
			} else {
				fileString = null;
				this.mensaje = "ERROR / No hay la llave publica para firmar el documento";
			}
		} catch (IOException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} catch (ParseException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			file.deleteOnExit();
		}
		
		return fileString;
	}
}
