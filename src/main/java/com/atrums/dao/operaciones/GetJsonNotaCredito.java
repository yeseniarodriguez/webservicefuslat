package com.atrums.dao.operaciones;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;

import com.atrums.modelo.JsonNotaCredito;
import com.atrums.modelo.Linea;

public class GetJsonNotaCredito {
	static final Logger log = Logger.getLogger(GetJsonNotaCredito.class);
	private JSONObject jsonObject = null;
	private JsonNotaCredito jsonNotaCredito = new JsonNotaCredito();
	private String mensaje = null;
	
	public GetJsonNotaCredito(JSONObject jsonObject) {
		this.jsonObject = jsonObject;
	}
	
	public boolean getJsonDatos() {
		try {
			if (this.jsonObject.getString("nrodocumento").length() > 0) {
				this.jsonNotaCredito.setNrodocumentoref(this.jsonObject.getString("nrodocumento"));
			} else {
				this.mensaje = "No hay el n�mero de documento en el Json enviado.";
				return false;
			}
		} catch (JSONException ex) {
			// TODO Auto-generated catch block
			this.mensaje = "No hay el n�mero de documento en el Json enviado.";
			return false;
		}
		
		try {
			if (this.jsonObject.getString("tipodoc").equals("04") || 
					this.jsonObject.getString("tipodoc").equals("4")) {
				this.jsonNotaCredito.setTipodoc(this.jsonObject.getString("tipodoc"));
			} else {
				this.mensaje = "El tipo de documento enviado no es una Nota de Cr�dito.";
				return false;
			}
		} catch (JSONException ex) {
			// TODO Auto-generated catch block
			this.mensaje = "No hay el tipo de documento en el Json enviado.";
			return false;
		}
		
		try {
			this.jsonNotaCredito.setNombreempresa(this.jsonObject.getString("nombreempresa").toUpperCase());
		} catch (JSONException ex) {
			// TODO Auto-generated catch block
			this.mensaje = "No hay el Nombre de la Empresa en el Json enviado.";
			return false;
		}
		
		try {
			this.jsonNotaCredito.setFechadocumento(this.jsonObject.getString("fechadocumento"));
		} catch (JSONException ex) {
			// TODO Auto-generated catch block
			this.mensaje = "No hay el Fecha del documento en el Json enviado.";
			return false;
		}
		
		try {
			this.jsonNotaCredito.setFechaNumDocRelacionado(this.jsonObject.getString("fechadocrelacionado"));
		} catch (JSONException ex) {
			// TODO Auto-generated catch block
			this.mensaje = "No hay el Fecha del documento original en el Json enviado.";
			return false;
		}
		
		try {
			if (this.jsonObject.getString("motivo").length() > 0) {
				this.jsonNotaCredito.setMotivo(this.jsonObject.getString("motivo"));
			} else {
				this.mensaje = "No hay el motivo de documento en el Json enviado.";
				return false;
			}
		} catch (JSONException ex) {
			// TODO Auto-generated catch block
			this.mensaje = "No hay el motivo de documento en el Json enviado.";
			return false;
		}
		
		try {
			this.jsonNotaCredito.setNroestablecimiento(this.jsonObject.getString("nroestablecimiento"));
		} catch (JSONException ex) {
			// TODO Auto-generated catch block
			this.mensaje = "No hay el nro. de establemicimento en el Json enviado.";
			return false;
		}
		
		try {
			this.jsonNotaCredito.setPtoemision(this.jsonObject.getString("ptoemision"));
		} catch (JSONException ex) {
			// TODO Auto-generated catch block
			this.mensaje = "No hay el pto. emisi�n en el Json enviado.";
			return false;
		}
		
		
		try {
			this.jsonNotaCredito.setCliente(this.jsonObject.getString("cliente"));
			
			
			if (this.jsonNotaCredito.getCliente().equals("9999999999999") || 
					this.jsonNotaCredito.getCliente().equals("999999999999") || 
					this.jsonNotaCredito.getCliente().equals("99999999999") || 
					this.jsonNotaCredito.getCliente().equals("9999999999")) {
				this.mensaje = "El ci/ruc es incorrecto del cliente que se ha especificado en el Json.";
				return false;
			}
			
			if (this.jsonNotaCredito.getCliente() == null) {
				this.mensaje = "No hay el CI/RUC del cliente en el Json enviado.";
				return false;
			} else if (this.jsonNotaCredito.getCliente().equals("")) {
				this.mensaje = "No hay el CI/RUC del cliente en el Json enviado.";
				return false;
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			this.mensaje = "No hay el CI/RUC del cliente en el Json enviado.";
			return false;
		}
		
		try {
			this.jsonNotaCredito.setNombrecliente(this.jsonObject.getString("nombrecliente").toUpperCase());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			this.mensaje = "No hay el nombre del cliente en el Json enviado.";
			return false;
		}
		
		try {
			this.jsonNotaCredito.setNumDocRelacionado(this.jsonObject.getString("numdocrelacionado"));
			
			if (this.jsonNotaCredito.getNumDocRelacionado().length() != 17) {
				// TODO Auto-generated catch block
				this.mensaje = "No esta el n�mero correcto de documento original en el Json enviado.";
				return false;
			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			this.mensaje = "No hay el n�mero de documento original en el Json enviado.";
			return false;
		}
		
		try {
			this.jsonNotaCredito.setTotaldocumento(this.jsonObject.getString("totaldocumento"));
		
			BigDecimal dbTotal = new BigDecimal(this.jsonNotaCredito.getTotaldocumento());
			
			if (this.jsonNotaCredito.getCliente().equals("999999999") || this.jsonNotaCredito.getCliente().equals("9999999999999")) {
				if (dbTotal.doubleValue() > 50) {
					this.mensaje = "No se puede emitir un documento que supere $50 a nombre del CONSUMIDOR FINAL.";
					return false;
				}
			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			this.mensaje = "No hay el total del documento en el Json enviado.";
			return false;
		}
		
		try {
			this.jsonNotaCredito.setTotalmodificado(this.jsonObject.getString("totalmodificado"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			this.mensaje = "No hay el total modicado original del documento en el Json enviado.";
			return false;
		}

		try {
			this.jsonNotaCredito.setEmailcliente(this.jsonObject.getString("emailcliente").toUpperCase());
			
			if (this.jsonNotaCredito.getEmailcliente().equals("")) {
				this.jsonNotaCredito.setEmailcliente(null);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
		}
		
		try {
			this.jsonNotaCredito.setDireccioncliente(this.jsonObject.getString("direccioncliente").toUpperCase());
			
			if (this.jsonNotaCredito.getDireccioncliente().equals("")) {
				this.jsonNotaCredito.setDireccioncliente(null);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
		}
		
		if (this.jsonNotaCredito.getCliente().equals("9999999999999")) {
			this.jsonNotaCredito.setEmailcliente("N/A");
			this.jsonNotaCredito.setDireccioncliente("N/A");
		} else if (this.jsonNotaCredito.getEmailcliente() == null) {
			this.mensaje = "No hay el email del cliente en el Json enviado.";
			return false;
		} else if (this.jsonNotaCredito.getDireccioncliente() == null) {
			this.mensaje = "No hay la direcci�n del cliente en el Json enviado.";
			return false;
		}
		
		try {
			for (int i=0; i<this.jsonObject.getJSONArray("lineas").length(); i++) {
				JSONObject auxLineaJson = (JSONObject) this.jsonObject.getJSONArray("lineas").get(i);
				
				Linea auxLinea = new Linea();
				
				try {
					auxLinea.setCodigoProducto(auxLineaJson.getString("codigoproducto").toUpperCase());
				} catch (JSONException ex) {
					this.mensaje = "No hay el c�digo del producto en las lineas en el Json enviado.";
					return false;
				}
				
				try {
					auxLinea.setCodigoimpuesto(auxLineaJson.getString("codigoimpuesto"));
				} catch (JSONException ex) {
					this.mensaje = "No hay el c�digo del impuesto en las lineas en el Json enviado.";
					return false;
				}
				
				try {
					auxLinea.setNombreProducto(auxLineaJson.getString("nombreproducto").toUpperCase());
				} catch (JSONException ex) {
					this.mensaje = "No hay el nombre del producto en las lineas en el Json enviado.";
					return false;
				}
				
				try {
					auxLinea.setPrecio(auxLineaJson.getString("preciounitario"));
				} catch (JSONException ex) {
					this.mensaje = "No hay el precio unitario en las lineas en el Json enviado.";
					return false;
				}
				
				try {
					auxLinea.setDescuento(auxLineaJson.getString("descuento"));
				} catch (JSONException ex) {
					this.mensaje = "No hay el descuento en las lineas en el Json enviado.";
					return false;
				}
				
				try {
					auxLinea.setCantidad(auxLineaJson.getString("cantidad"));
				} catch (JSONException ex) {
					this.mensaje = "No hay la cantidad en las lineas en el Json enviado.";
					return false;
				}
				
				try {
					auxLinea.setPorcentajeimpuesto(auxLineaJson.getString("porcentajeimpuesto"));
				} catch (JSONException ex) {
					this.mensaje = "No hay el % del impuesto en las lineas en el Json enviado.";
					return false;
				}
				
				try {
					auxLinea.setTotallinea(auxLineaJson.getString("totallinea"));
				} catch (JSONException ex) {
					this.mensaje = "No hay el total de la linea en las lineas en el Json enviado.";
					return false;
				}
				
				try {
					auxLinea.setImpuestolinea(auxLineaJson.getString("impuestolinea"));
				} catch (JSONException ex) {
					this.mensaje = "No hay el total del impuesto de la linea en las lineas en el Json enviado.";
					return false;
				}
				
				try {
					auxLinea.setTipoimpuesto(auxLineaJson.getString("tipoimpuesto").toUpperCase());
					
					if (!auxLinea.getTipoimpuesto().equals("IVA") && 
							!auxLinea.getTipoimpuesto().equals("ICE") && 
							!auxLinea.getTipoimpuesto().equals("IRBPNR")) {
						this.mensaje = "No hay el tipo de impuesto que se encuentra en las lineas en el Json enviado.";
						return false;
					}
					
				} catch (JSONException ex) {
					this.mensaje = "No hay el tipo de impuesto de la linea en las lineas en el Json enviado.";
					return false;
				}
				
				this.jsonNotaCredito.getLineas().add(auxLinea);
			}
			
			if (this.jsonNotaCredito.getLineas().isEmpty()) {
				this.mensaje = "No hay lineas en el Json enviado.";
				return false;
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			this.mensaje = "No hay lineas en el Json enviado.";
			return false;
		}
		
		return true;
	}

	public JsonNotaCredito getJsonNotaCredito() {
		return jsonNotaCredito;
	}

	public void setJsonNotaCredito(JsonNotaCredito jsonNotaCredito) {
		this.jsonNotaCredito = jsonNotaCredito;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	
}
