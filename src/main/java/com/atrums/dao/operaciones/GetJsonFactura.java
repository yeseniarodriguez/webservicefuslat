package com.atrums.dao.operaciones;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;

import com.atrums.modelo.JsonFactura;
import com.atrums.modelo.Linea;
import com.atrums.modelo.Pago;

public class GetJsonFactura {
	static final Logger log = Logger.getLogger(GetJsonFactura.class);
	private JSONObject jsonObject = null;
	private JsonFactura jsonFactura = new JsonFactura();
	private String mensaje = null;
	
	public GetJsonFactura(JSONObject jsonObject) {
		this.jsonObject = jsonObject;
	}
	
	public boolean getJsonDatos() {
		try {
			if (this.jsonObject.getString("nrodocumento").length() > 0) {
				this.jsonFactura.setNrodocumentoref(this.jsonObject.getString("nrodocumento"));
			} else {
				this.mensaje = "No hay el n�mero de documento en el Json enviado.";
				return false;
			}
		} catch (JSONException ex) {
			// TODO Auto-generated catch block
			this.mensaje = "No hay el n�mero de documento en el Json enviado.";
			return false;
		}
		
		try {
			if (this.jsonObject.getString("tipodoc").equals("01") || 
					this.jsonObject.getString("tipodoc").equals("1")) {
				this.jsonFactura.setTipodoc(this.jsonObject.getString("tipodoc"));
			} else {
				this.mensaje = "El tipo de documento enviado no es una factura.";
				return false;
			}
		} catch (JSONException ex) {
			// TODO Auto-generated catch block
			this.mensaje = "No hay el tipo de documento en el Json enviado.";
			return false;
		}
		
		try {
			this.jsonFactura.setNombreempresa(this.jsonObject.getString("nombreempresa").toUpperCase());
		} catch (JSONException ex) {
			// TODO Auto-generated catch block
			this.mensaje = "No hay el Nombre de la Empresa en el Json enviado.";
			return false;
		}
		
		try {
			this.jsonFactura.setFechadocumento(this.jsonObject.getString("fechadocumento"));
		} catch (JSONException ex) {
			// TODO Auto-generated catch block
			this.mensaje = "No hay el Fecha del documento en el Json enviado.";
			return false;
		}
		
		try {
			this.jsonFactura.setNroestablecimiento(this.jsonObject.getString("nroestablecimiento"));
		} catch (JSONException ex) {
			// TODO Auto-generated catch block
			this.mensaje = "No hay el nro. de establemicimento en el Json enviado.";
			return false;
		}
		
		try {
			this.jsonFactura.setPtoemision(this.jsonObject.getString("ptoemision"));
		} catch (JSONException ex) {
			// TODO Auto-generated catch block
			this.mensaje = "No hay el pto. emisi�n en el Json enviado.";
			return false;
		}
		
		
		try {
			this.jsonFactura.setCliente(this.jsonObject.getString("cliente"));
			
			if (this.jsonFactura.getCliente().equals("9999999999999") || 
					this.jsonFactura.getCliente().equals("999999999999") || 
					this.jsonFactura.getCliente().equals("99999999999") || 
					this.jsonFactura.getCliente().equals("9999999999")) {
				this.mensaje = "El ci/ruc es incorrecto del cliente que se ha especificado en el Json.";
				return false;
			}
			
			if (this.jsonFactura.getCliente() == null) {
				this.mensaje = "No hay el CI/RUC del cliente en el Json enviado.";
				return false;
			} else if (this.jsonFactura.getCliente().equals("")) {
				this.mensaje = "No hay el CI/RUC del cliente en el Json enviado.";
				return false;
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			this.mensaje = "No hay el CI/RUC del cliente en el Json enviado.";
			return false;
		}
		
		try {
			this.jsonFactura.setNombrecliente(this.jsonObject.getString("nombrecliente").toUpperCase());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			this.mensaje = "No hay el nombre del cliente en el Json enviado.";
			return false;
		}
		
		try {
			this.jsonFactura.setPropina(this.jsonObject.getString("propina"));
			
			if (this.jsonFactura.getPropina().equals("")) {
				this.jsonFactura.setPropina("0.00");
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			this.jsonFactura.setPropina("0.00");
		}
		
		try {
			this.jsonFactura.setTotaldocumento(this.jsonObject.getString("totaldocumento"));
			
			BigDecimal dbTotal = new BigDecimal(this.jsonFactura.getTotaldocumento());
			
			if (this.jsonFactura.getCliente().equals("999999999") || this.jsonFactura.getCliente().equals("9999999999999")) {
				if (dbTotal.doubleValue() > 50) {
					this.mensaje = "No se puede emitir un documento que supere $50 a nombre del CONSUMIDOR FINAL.";
					return false;
				}
			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			this.mensaje = "No hay el total del documento en el Json enviado.";
			return false;
		}
		
		try {
			this.jsonFactura.setEmailcliente(this.jsonObject.getString("emailcliente").toUpperCase());
			
			if (this.jsonFactura.getEmailcliente().equals("")) {
				this.jsonFactura.setEmailcliente(null);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
		}
		
		try {
			this.jsonFactura.setDireccioncliente(this.jsonObject.getString("direccioncliente").toUpperCase());
			
			if (this.jsonFactura.getDireccioncliente().equals("")) {
				this.jsonFactura.setDireccioncliente(null);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
		}
		
		if (this.jsonFactura.getCliente().equals("9999999999999")) {
			this.jsonFactura.setEmailcliente("N/A");
			this.jsonFactura.setDireccioncliente("N/A");
		} else if (this.jsonFactura.getEmailcliente() == null) {
			this.mensaje = "No hay el email del cliente en el Json enviado.";
			return false;
		} else if (this.jsonFactura.getDireccioncliente() == null) {
			this.mensaje = "No hay la direcci�n del cliente en el Json enviado.";
			return false;
		}
		
		try {
			
			this.jsonFactura.setLineas(new ArrayList<Linea>());
			
			for (int i=0; i<this.jsonObject.getJSONArray("lineas").length(); i++) {
				JSONObject auxLineaJson = (JSONObject) this.jsonObject.getJSONArray("lineas").get(i);
				
				Linea auxLinea = new Linea();
				
				try {
					auxLinea.setCodigoProducto(auxLineaJson.getString("codigoproducto").toUpperCase());
				} catch (JSONException ex) {
					this.mensaje = "No hay el c�digo del producto en las lineas en el Json enviado.";
					return false;
				}
				
				try {
					auxLinea.setCodigoimpuesto(auxLineaJson.getString("codigoimpuesto"));
				} catch (JSONException ex) {
					this.mensaje = "No hay el c�digo del impuesto en las lineas en el Json enviado.";
					return false;
				}
				
				try {
					auxLinea.setNombreProducto(auxLineaJson.getString("nombreproducto").toUpperCase());
				} catch (JSONException ex) {
					this.mensaje = "No hay el nombre del producto en las lineas en el Json enviado.";
					return false;
				}
				
				try {
					auxLinea.setPrecio(auxLineaJson.getString("preciounitario"));
				} catch (JSONException ex) {
					this.mensaje = "No hay el precio unitario en las lineas en el Json enviado.";
					return false;
				}
				
				try {
					auxLinea.setDescuento(auxLineaJson.getString("descuento"));
				} catch (JSONException ex) {
					this.mensaje = "No hay el descuento en las lineas en el Json enviado.";
					return false;
				}
				
				try {
					auxLinea.setCantidad(auxLineaJson.getString("cantidad"));
				} catch (JSONException ex) {
					this.mensaje = "No hay la cantidad en las lineas en el Json enviado.";
					return false;
				}
				
				try {
					auxLinea.setPorcentajeimpuesto(auxLineaJson.getString("porcentajeimpuesto"));
				} catch (JSONException ex) {
					this.mensaje = "No hay el % del impuesto en las lineas en el Json enviado.";
					return false;
				}
				
				try {
					auxLinea.setTotallinea(auxLineaJson.getString("totallinea"));
				} catch (JSONException ex) {
					this.mensaje = "No hay el total de la linea en las lineas en el Json enviado.";
					return false;
				}
				
				try {
					auxLinea.setImpuestolinea(auxLineaJson.getString("impuestolinea"));
				} catch (JSONException ex) {
					this.mensaje = "No hay el total del impuesto de la linea en las lineas en el Json enviado.";
					return false;
				}
				
				try {
					auxLinea.setTipoimpuesto(auxLineaJson.getString("tipoimpuesto").toUpperCase());
					
					if (!auxLinea.getTipoimpuesto().equals("IVA") && 
							!auxLinea.getTipoimpuesto().equals("ICE") && 
							!auxLinea.getTipoimpuesto().equals("IRBPNR")) {
						this.mensaje = "No hay el tipo de impuesto que se encuentra en las lineas en el Json enviado.";
						return false;
					}
					
				} catch (JSONException ex) {
					this.mensaje = "No hay el tipo de impuesto de la linea en las lineas en el Json enviado.";
					return false;
				}
				
				this.jsonFactura.getLineas().add(auxLinea);
			}
			
			if (this.jsonFactura.getLineas().isEmpty()) {
				this.mensaje = "No hay lineas en el Json enviado.";
				return false;
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			this.mensaje = "No hay lineas en el Json enviado.";
			return false;
		}
		
		try {
			for (int i=0; i<this.jsonObject.getJSONArray("pagos").length(); i++) {
				JSONObject auxPagoJson = (JSONObject) this.jsonObject.getJSONArray("pagos").get(i);
				
				Pago auxPago = new Pago();
				
				try {
					auxPago.setMetodoPago(auxPagoJson.getString("metodopago").toUpperCase());
				} catch (JSONException ex) {
					this.mensaje = "No hay el m�todo de pago en los pagos en el Json enviado.";
					return false;
				}
				
				try {
					auxPago.setTotalPago(auxPagoJson.getString("totalpago"));
				} catch (JSONException ex) {
					this.mensaje = "No hay el total del pago en los pagos en el Json enviado.";
					return false;
				}
				
				this.jsonFactura.getPagos().add(auxPago);
			}
			
			if (this.jsonFactura.getPagos().isEmpty()) {
				this.mensaje = "No hay pagos en el Json enviado.";
				return false;
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			this.mensaje = "No hay pagos en el Json enviado.";
			return false;
		}
		
		return true;
	}

	public JsonFactura getJsonFactura() {
		return jsonFactura;
	}

	public void setJsonFactura(JsonFactura jsonFactura) {
		this.jsonFactura = jsonFactura;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
}
