package com.atrums.dao.operaciones;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.atrums.modelo.ConfDinamica;
import com.atrums.modelo.Configuracion;
import com.atrums.modelo.Invoice;
import com.atrums.persistencia.OperacionesBDDPostgres;

public class OperacionesEnvioEmail implements Runnable{
	static final Logger log = Logger.getLogger(OperacionesEnvioEmail.class);
	private Invoice invoice = null;
	private Configuracion configuracion = new Configuracion();
	private ConfDinamica confDinamica = null;
	private OperacionesAuxiliares auxiliares = new OperacionesAuxiliares();
	private OperacionesBDDPostgres bddPostgres = new OperacionesBDDPostgres();
	
	private String correo;
	private String cliente;
	private String tipoDoc;
	private Connection connection;
	
	public OperacionesEnvioEmail(Invoice invoice, String correo,  
			String cliente, 
			String tipoDoc, 
			DataSource dataSource) {
		this.invoice = invoice;
		this.correo = correo;
		this.cliente = cliente;
		this.tipoDoc = tipoDoc;
		this.connection = this.bddPostgres.getConneccion(dataSource);
	}

	public void run() {
		// TODO Auto-generated method stub
		try {
			log.info("Enviando Email");
			
			Properties props = new Properties();
			 
			props.setProperty("mail.smtp.host", this.invoice.getPocEmail().getSmtpserver());
			props.setProperty("mail.smtp.port", this.invoice.getPocEmail().getSmtpport());
			props.setProperty("mail.smtp.auth", this.invoice.getPocEmail().isSmtpautorization() ? "true" : "false");
			props.setProperty("mail.smtp.starttls.enable", "true");
			
			Session session = Session.getDefaultInstance(props, null);

            BodyPart texto = new MimeBodyPart();
            
            String cuerpoCorreo = this.configuracion.getCuerpoCorreo();
            
            this.confDinamica = new ConfDinamica(this.invoice.getInfoTributaria().getRuc());
            
            cuerpoCorreo = cuerpoCorreo.replace("user", cliente);
            cuerpoCorreo = cuerpoCorreo.replace("pass", cliente);
            cuerpoCorreo = cuerpoCorreo.replace("ServerConsultas", this.confDinamica.getConsulta());
            		
            texto.setText(cuerpoCorreo);
            BodyPart adjunto = new MimeBodyPart();
            
            adjunto.setDataHandler(
                new DataHandler(new FileDataSource(this.invoice.getFileXML().getPath())));
            adjunto.setFileName("documento - " + this.invoice.getClaveacceso() + ".xml");
            
            BodyPart adjunto2 = new MimeBodyPart();
            
            String baseDesign = "./..//REPORTES/"; 
            String reporte = (tipoDoc.equals("01") ? "Rpt_Factura.jrxml" : "") + 
            (tipoDoc.equals("04") ? "Rpt_NotaCredito.jrxml" : "") +  
            (tipoDoc.equals("RT") ? "Rpt_Retenciones.jrxml" : "");
            
            String dirBaseDesign = baseDesign + reporte; 
            
            URL url = this.getClass().getClassLoader().getResource(dirBaseDesign);
    		baseDesign = url.getPath().replace(reporte, "");
    		
    		if(baseDesign.indexOf(":") != -1){
    			baseDesign = baseDesign.replaceFirst("/", "");
    		}else if(baseDesign.indexOf("//") != -1){
    			baseDesign = baseDesign.replaceFirst("//", "/");
    		}
            
    		log.info("Cargando datos de  Email");
    		File pdf = auxiliares.generarPDF(baseDesign, reporte, this.invoice.getcInvoiceId(), this.invoice.getClaveacceso(), connection);

            adjunto2.setDataHandler(
	                new DataHandler(new FileDataSource(pdf.getPath())));
	            adjunto2.setFileName("documento - " + this.invoice.getClaveacceso() + ".pdf");
            
            MimeMultipart multiParte = new MimeMultipart();
            multiParte.addBodyPart(texto);
            multiParte.addBodyPart(adjunto);
            multiParte.addBodyPart(adjunto2);
            
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(this.invoice.getPocEmail().getSmtpserveracount()));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(correo));
            message.setSubject(MimeUtility.encodeText(this.configuracion.getAsunto(),"UTF-8","B"));
            message.setContent(multiParte);
            
            Transport t = session.getTransport("smtp");
            
			//conectando al email
            
            String auxCuenta = this.invoice.getPocEmail().getSmtpserveracount();
            String auxPass = auxiliares.dencryptSh1(this.invoice.getPocEmail().getSmtppassword());
            
			t.connect(auxCuenta, auxPass);
            
			//enviando email
			t.sendMessage(message, message.getAllRecipients());
			
			//cerrando conexion
            t.close();
            log.info("Email Enviado");
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			StringWriter stack = new StringWriter();
			ex.printStackTrace(new PrintWriter(stack));
			log.warn("Caught exception; decorating with appropriate status template : " + stack.toString());
		} finally {
			try {if (this.connection != null) this.connection.close();} catch (SQLException ex) {}
		}
	}
}
