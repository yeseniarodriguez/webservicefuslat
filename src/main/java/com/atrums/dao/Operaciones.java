package com.atrums.dao;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;

import com.atrums.dao.operaciones.GetJsonFactura;
import com.atrums.dao.operaciones.GetJsonNotaCredito;
import com.atrums.dao.proceso.OperacionesOpenbravo;
import com.atrums.dao.proceso.OperacionesSRI;
import com.atrums.modelo.Respuesta;
import com.atrums.persistencia.Conexion;
import com.atrums.persistencia.OperacionesBDDPostgres;

@Path("/")
public class Operaciones {
	static final Logger log = Logger.getLogger(Operaciones.class);
	private DataSource dataSource = Conexion.getDataSource();
	private OperacionesBDDPostgres bddPostgres = new OperacionesBDDPostgres();
	
	@GET
	@Path("/facturaElectronica")
	@Produces(MediaType.APPLICATION_JSON)
	public Respuesta facturaElectronicaGet(@QueryParam("fac") String inputJsonObj) {
		Respuesta auxRespuesta = new Respuesta();
		
		return auxRespuesta;
	}
	
	@POST
	@Path("/facturaElectronica")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Respuesta facturaElectronicaPost(String inputJsonObj) {
		log.info("Json a procesar: " + inputJsonObj);
		Respuesta auxRespuesta = new Respuesta();
		GetJsonFactura getJsonFactura;
		Connection connection = null;
		
		try {
			log.info("Conectando a BDD");
			connection = this.bddPostgres.getConneccion(this.dataSource);
			
			log.info("Realizando un Test");
			if (!this.bddPostgres.isTestConextion(connection)) {
				try {if (connection != null) connection.close();} catch (SQLException ex) {}
				Conexion.createNewDataSource();
				this.dataSource = Conexion.getDataSource();
				connection = this.bddPostgres.getConneccion(this.dataSource);
				
				if (!this.bddPostgres.isTestConextion(connection)) {
					log.warn("Hay fallas con la bdd del sistema de facturaci�n electr�nica de ATRUMS por favor intentelo m�s tarde, o comuniquese con Soporte.");
					auxRespuesta.setMensaje("Documento No Autorizado");
					auxRespuesta.setMensajeerror("Hay fallas con la bdd del sistema de facturaci�n electr�nica de ATRUMS por favor intentelo m�s tarde, o comuniquese con Soporte.");
					auxRespuesta.setProcesado(false);
					
					return auxRespuesta;
				}
			}
			
			log.info("Verificando Json");
			if (inputJsonObj == null) {
				connection.rollback();
				this.bddPostgres.putLog(null, null, "Error: Error en JSON enviado - Json vacio", null, "RZ", connection);
				log.warn("Error en JSON enviado - Json vacio");
				auxRespuesta.setMensaje("Documento No Procesado");
				auxRespuesta.setMensajeerror("Error en JSON enviado - Json vacio");
				auxRespuesta.setProcesado(false);
				
				return auxRespuesta;
			} else if (inputJsonObj.equals("")) {
				connection.rollback();
				this.bddPostgres.putLog(null, null, "Error: Error en JSON enviado - Json vacio", null, "RZ", connection);
				log.warn("Error en JSON enviado - Json vacio");
				auxRespuesta.setMensaje("Documento No Procesado");
				auxRespuesta.setMensajeerror("Error en JSON enviado - Json vacio");
				auxRespuesta.setProcesado(false);
				
				return auxRespuesta;
			}
			
			getJsonFactura = new GetJsonFactura(new JSONObject(inputJsonObj));
			
			log.info("Cargando Datos de JSON");
			if (getJsonFactura.getJsonDatos()) {
				OperacionesSRI operacionesSRI = new OperacionesSRI(getJsonFactura.getJsonFactura(), connection);
				
				log.info("Cargando Procesando con el SRI");
				if (operacionesSRI.procesarSRI()) {
					OperacionesOpenbravo operacionesOpenbravo = new OperacionesOpenbravo(getJsonFactura.getJsonFactura(), 
							operacionesSRI.getDocxml(), 
							operacionesSRI.getFilexml(),
							operacionesSRI.getDocAutInvoice(), 
							connection);
					
					log.info("Cargando Procesando con el Openbravo");
					if (operacionesOpenbravo.procesarOpenbravo()) {
						connection.rollback();
						this.bddPostgres.putLog(getJsonFactura.getJsonFactura().getNrodocumentoref(), 
								getJsonFactura.getJsonFactura().getNroestablecimiento() + "-" + 
								getJsonFactura.getJsonFactura().getPtoemision() + "-" + 
								getJsonFactura.getJsonFactura().getNrodocumentoopen(), 
								"Recibido", 
								inputJsonObj, 
								"RC", 
								connection);
						log.warn("Documento Procesado para Autorizar SRI Offline");
						auxRespuesta.setJsonFactura(getJsonFactura.getJsonFactura());
						auxRespuesta.setMensaje("Documento Procesado para Autorizar SRI Offline");
						auxRespuesta.setMensajeerror("");
						auxRespuesta.setProcesado(true);						
					} else {
						log.warn("Devolviendo Error");
						connection.rollback();
						this.bddPostgres.putLog(getJsonFactura.getJsonFactura().getNrodocumentoref(), 
								getJsonFactura.getJsonFactura().getNroestablecimiento() + "-" + 
								getJsonFactura.getJsonFactura().getPtoemision() + "-" + 
								(getJsonFactura.getJsonFactura().getNrodocumentoopen() == null ? "" : getJsonFactura.getJsonFactura().getNrodocumentoopen()), 
								"Error: " + operacionesOpenbravo.getMensaje(), 
								inputJsonObj, 
								"RZ", 
								connection);
						log.warn(operacionesOpenbravo.getMensaje());
						auxRespuesta.setJsonFactura(getJsonFactura.getJsonFactura());
						auxRespuesta.setMensaje("Documento No Procesado");
						auxRespuesta.setMensajeerror(operacionesOpenbravo.getMensaje());
						auxRespuesta.setProcesado(false);
					}
				} else {
					log.warn("Devolviendo Error");
					connection.rollback();
					this.bddPostgres.putLog(getJsonFactura.getJsonFactura().getNrodocumentoref(), 
							getJsonFactura.getJsonFactura().getNroestablecimiento() + "-" + 
							getJsonFactura.getJsonFactura().getPtoemision() + "-" + 
							(getJsonFactura.getJsonFactura().getNrodocumentoopen() == null ? "" : getJsonFactura.getJsonFactura().getNrodocumentoopen()), 
							"Error: " + operacionesSRI.getMensaje(), 
							inputJsonObj, 
							"RZ", 
							connection);
					log.warn(operacionesSRI.getMensaje());
					auxRespuesta.setJsonFactura(getJsonFactura.getJsonFactura());
					auxRespuesta.setMensaje("Documento Procesado para el SRI Offline");
					auxRespuesta.setMensajeerror(operacionesSRI.getMensaje());
					auxRespuesta.setProcesado(false);
				}
			} else {
				log.warn("Devolviendo Error");
				connection.rollback();
				this.bddPostgres.putLog(getJsonFactura.getJsonFactura().getNrodocumentoref(), 
						null,
						"Error: " + getJsonFactura.getMensaje(), 
						inputJsonObj, 
						"RZ", 
						connection);
				log.warn(getJsonFactura.getMensaje());
				auxRespuesta.setJsonFactura(getJsonFactura.getJsonFactura());
				auxRespuesta.setMensajeerror(getJsonFactura.getMensaje());
				auxRespuesta.setMensaje("Documento No Procesado");
				auxRespuesta.setProcesado(false);
			}
		} catch (JSONException ex) {
			// TODO Auto-generated catch block
			log.warn("Devolviendo Error");
			StringWriter stack = new StringWriter();
			ex.printStackTrace(new PrintWriter(stack));
			log.warn("Caught exception; decorating with appropriate status template : " + stack.toString());
			auxRespuesta.setMensajeerror("Error en JSON enviado");
			auxRespuesta.setMensaje("Documento No Procesado");
			auxRespuesta.setProcesado(false);
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn("Devolviendo Error");
			StringWriter stack = new StringWriter();
			ex.printStackTrace(new PrintWriter(stack));
			log.warn("Caught exception; decorating with appropriate status template : " + stack.toString());
			auxRespuesta.setMensajeerror("Error en JSON enviado");
			auxRespuesta.setMensaje("Documento No Procesado");
			auxRespuesta.setProcesado(false);
		} finally {
			try {if (connection != null) connection.close();} catch (SQLException ex) {}
		}
		
		return auxRespuesta;
	}
	
	@GET
	@Path("/notaCreditoElectronica")
	@Produces(MediaType.APPLICATION_JSON)
	public Respuesta notaCreditoElectronicaGet(@QueryParam("noc") String inputJsonObj) {
		Respuesta auxRespuesta = new Respuesta();
		
		
		
		return auxRespuesta;
	}
	
	@POST
	@Path("/notaCreditoElectronica")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Respuesta notaCreditoElectronicaPost(String inputJsonObj) {
		log.info("Json a procesar: " + inputJsonObj);
		
		Respuesta auxRespuesta = new Respuesta();
		GetJsonNotaCredito getJsonNotaCredito;
		Connection connection = null;
		
		try {
			connection = this.bddPostgres.getConneccion(this.dataSource);
			
			if (!this.bddPostgres.isTestConextion(connection)) {
				try {if (connection != null) connection.close();} catch (SQLException ex) {}
				Conexion.createNewDataSource();
				this.dataSource = Conexion.getDataSource();
				connection = this.bddPostgres.getConneccion(this.dataSource);
				
				if (!this.bddPostgres.isTestConextion(connection)) {
					log.warn("Hay fallas con la bdd del sistema de facturaci�n electr�nica de ATRUMS por favor intentelo m�s tarde, o comuniquese con Soporte.");
					auxRespuesta.setMensaje("Documento No Autorizado");
					auxRespuesta.setMensajeerror("Hay fallas con la bdd del sistema de facturaci�n electr�nica de ATRUMS por favor intentelo m�s tarde, o comuniquese con Soporte.");
					auxRespuesta.setProcesado(false);
					
					return auxRespuesta;
				}
			}
			
			if (inputJsonObj == null) {
				connection.rollback();
				this.bddPostgres.putLog(null, null, "Error: Error en JSON enviado - Json vacio", null, "RZ", connection);
				log.warn("Error en JSON enviado - Json vacio");
				auxRespuesta.setMensaje("Documento No Procesado");
				auxRespuesta.setMensajeerror("Error en JSON enviado - Json vacio");
				auxRespuesta.setProcesado(false);
				
				return auxRespuesta;
			} else if (inputJsonObj.equals("")) {
				connection.rollback();
				this.bddPostgres.putLog(null, null, "Error: Error en JSON enviado - Json vacio", null, "RZ", connection);
				log.warn("Error en JSON enviado - Json vacio");
				auxRespuesta.setMensaje("Documento No Procesado");
				auxRespuesta.setMensajeerror("Error en JSON enviado - Json vacio");
				auxRespuesta.setProcesado(false);
				
				return auxRespuesta;
			}
			
			getJsonNotaCredito = new GetJsonNotaCredito(new JSONObject(inputJsonObj));
			
			if (getJsonNotaCredito.getJsonDatos()) {
				OperacionesSRI operacionesSRI = new OperacionesSRI(getJsonNotaCredito.getJsonNotaCredito(), connection);
				
				if (operacionesSRI.procesarNotaSRI()) {
					OperacionesOpenbravo operacionesOpenbravo = new OperacionesOpenbravo(getJsonNotaCredito.getJsonNotaCredito(), 
							operacionesSRI.getDocxml(), 
							operacionesSRI.getFilexml(),
							operacionesSRI.getDocAutInvoice(), 
							connection);
					
					if (operacionesOpenbravo.procesarNotaOpenbravo()) {
						connection.rollback();
						this.bddPostgres.putLog(getJsonNotaCredito.getJsonNotaCredito().getNrodocumentoref(), 
								getJsonNotaCredito.getJsonNotaCredito().getNroestablecimiento() + "-" + 
								getJsonNotaCredito.getJsonNotaCredito().getPtoemision() + "-" + 
								getJsonNotaCredito.getJsonNotaCredito().getNrodocumentoopen(), 
								"Recibido", 
								inputJsonObj, 
								"RC", 
								connection);
						log.warn("Documento Procesado para Autorizar SRI Offline");
						auxRespuesta.setJsonNotaCredito(getJsonNotaCredito.getJsonNotaCredito());
						auxRespuesta.setMensaje("Documento Procesado para el SRI Offline");
						auxRespuesta.setMensajeerror("");
						auxRespuesta.setProcesado(true);
					} else {
						connection.rollback();
						this.bddPostgres.putLog(getJsonNotaCredito.getJsonNotaCredito().getNrodocumentoref(), 
								getJsonNotaCredito.getJsonNotaCredito().getNroestablecimiento() + "-" + 
								getJsonNotaCredito.getJsonNotaCredito().getPtoemision() + "-" + 
								(getJsonNotaCredito.getJsonNotaCredito().getNrodocumentoopen() == null ? "" : getJsonNotaCredito.getJsonNotaCredito().getNrodocumentoopen()), 
								"Error: " + operacionesOpenbravo.getMensaje(), 
								inputJsonObj, 
								"RZ", 
								connection);
						log.warn(operacionesSRI.getMensaje());
						log.warn(operacionesOpenbravo.getMensaje());
						auxRespuesta.setJsonNotaCredito(getJsonNotaCredito.getJsonNotaCredito());
						auxRespuesta.setMensaje("Documento No Procesado");
						auxRespuesta.setMensajeerror(operacionesOpenbravo.getMensaje());
						auxRespuesta.setProcesado(false);
					}
				} else {
					connection.rollback();
					this.bddPostgres.putLog(getJsonNotaCredito.getJsonNotaCredito().getNrodocumentoref(), 
							getJsonNotaCredito.getJsonNotaCredito().getNroestablecimiento() + "-" + 
							getJsonNotaCredito.getJsonNotaCredito().getPtoemision() + "-" + 
							(getJsonNotaCredito.getJsonNotaCredito().getNrodocumentoopen() == null ? "" : getJsonNotaCredito.getJsonNotaCredito().getNrodocumentoopen()), 
							"Error: " + operacionesSRI.getMensaje(), 
							inputJsonObj, 
							"RZ", 
							connection);
					log.warn(operacionesSRI.getMensaje());
					auxRespuesta.setJsonNotaCredito(getJsonNotaCredito.getJsonNotaCredito());
					auxRespuesta.setMensaje("Documento Procesado para el SRI Offline");
					auxRespuesta.setMensajeerror(operacionesSRI.getMensaje());
					auxRespuesta.setProcesado(false);
				}
			} else {
				connection.rollback();
				this.bddPostgres.putLog(getJsonNotaCredito.getJsonNotaCredito().getNrodocumentoref(), 
						null, 
						"Error: " + getJsonNotaCredito.getMensaje(), 
						inputJsonObj, 
						"RZ", 
						connection);
				log.warn(getJsonNotaCredito.getMensaje());
				auxRespuesta.setJsonNotaCredito(getJsonNotaCredito.getJsonNotaCredito());
				auxRespuesta.setMensajeerror(getJsonNotaCredito.getMensaje());
				auxRespuesta.setMensaje("Documento No Procesado");
				auxRespuesta.setProcesado(false);
			}
		} catch (JSONException ex) {
			// TODO Auto-generated catch block
			StringWriter stack = new StringWriter();
			ex.printStackTrace(new PrintWriter(stack));
			log.warn("Caught exception; decorating with appropriate status template : " + stack.toString());
			auxRespuesta.setMensajeerror("Error en JSON enviado");
			auxRespuesta.setMensaje("Documento No Autorizado");
			auxRespuesta.setProcesado(false);
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			StringWriter stack = new StringWriter();
			ex.printStackTrace(new PrintWriter(stack));
			log.warn("Caught exception; decorating with appropriate status template : " + stack.toString());
			auxRespuesta.setMensajeerror("Error en JSON enviado");
			auxRespuesta.setMensaje("Documento No Autorizado");
			auxRespuesta.setProcesado(false);
		} finally {
			try {if (connection != null) connection.close();} catch (SQLException ex) {}
		}
		
		return auxRespuesta;
	}
}
