package com.atrums.dao.proceso;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.atrums.dao.operaciones.GenerarXMLFactura;
import com.atrums.dao.operaciones.GenerarXMLNotaCredito;
import com.atrums.modelo.Client;
//import com.atrums.modelo.ConfService;
import com.atrums.modelo.Invoice;
import com.atrums.modelo.JsonFactura;
import com.atrums.modelo.JsonNotaCredito;
import com.atrums.modelo.Linea;
//import com.atrums.modelo.SRI.SRIDocumentoAutorizado;
//import com.atrums.modelo.SRI.SRIDocumentoRecibido;
import com.atrums.persistencia.OperacionesBDDPostgres;
//import com.atrums.persistencia.ServiceAutorizacion;
//import com.atrums.persistencia.ServiceRecibido;

public class OperacionesSRI {
	static final Logger log = Logger.getLogger(OperacionesSRI.class);
	private JsonFactura jsonFactura = null;
	private JsonNotaCredito jsonNotaCredito = null;
	private String mensaje = null;
	private String docxml = null;
	private File filexml = null;
	private OperacionesBDDPostgres bddPostgres = new OperacionesBDDPostgres();
	private Connection connection = null;
	
	private Invoice docAutInvoice = new Invoice();
	
	public OperacionesSRI(JsonFactura jsonFactura, Connection connection) {
		this.jsonFactura = jsonFactura;
		this.connection = connection;
	}
	
	public Invoice getDocAutInvoice() {
		return docAutInvoice;
	}

	public void setDocAutInvoice(Invoice docAutInvoice) {
		this.docAutInvoice = docAutInvoice;
	}

	public File getFilexml() {
		return filexml;
	}

	public void setFilexml(File filexml) {
		this.filexml = filexml;
	}

	public OperacionesSRI(JsonNotaCredito jsonNotaCredito , Connection connection) {
		this.jsonNotaCredito = jsonNotaCredito;
		this.connection = connection;
	}
	
	public String getDocxml() {
		return docxml;
	}

	public void setDocxml(String docxml) {
		this.docxml = docxml;
	}

	public boolean procesarSRI() {
		log.info("Creando XML");
		String xml = null;
		
		try {
			if (!this.jsonFactura.getTipodoc().equals("01")) {
				this.mensaje = "No es una factura el tipo de documento que se ha especificado en el Json.";
				return false;
			}
			
			if (this.connection == null) {
				this.mensaje = "Hay fallas con la bdd del sistema de facturación electrónica de ATRUMS por favor intentelo más tarde, o comuniquese con Soporte.";
				return false;
			}
			
			/****************************Cambio a Crear Borrador Inicio*****************************************************/
			
			List<String> adClients = this.bddPostgres.getAdClientId(this.jsonFactura.getNombreempresa(), this.connection);
			
			if (adClients.isEmpty()) {
				this.mensaje = "No existe la empresa en el sistema de facturación electrónica de ATRUMS que se ha especificado en el Json, comuniquese con Soporte.";
				return false;
			} else if (adClients.size() > 1) {
				this.mensaje = "Hay más de una empresa en el sistema de facturación electrónica que se ha especificado con el mismo nombre en el Json, comuniquese con Soporte.";
				return false;
			} else {
				this.docAutInvoice.setAdClientId(adClients.get(0));
				this.docAutInvoice.setSucursal(this.jsonFactura.getNroestablecimiento());
				this.docAutInvoice.setPtoemision(this.jsonFactura.getPtoemision());
				
				this.bddPostgres.setAuxInvoice(this.docAutInvoice);
			}
			
			List<String> adOrgs = this.bddPostgres.getAdOrgId(this.connection);
			
			if (adOrgs.isEmpty()) {
				this.mensaje = "No existe la sucursal en el sistema de facturación electrónica de ATRUMS que se ha especificado en el Json, "
						+ "que tiene Nro. Establecimiento: " + this.jsonFactura.getNroestablecimiento() + " y Pto. Emisión: " + this.jsonFactura.getPtoemision() + ", comuniquese con Soporte.";
				return false;
			} else if (adOrgs.size() > 1) {
				this.mensaje = "Hay más de una sucursal en el sistema de facturación electrónica de ATRUMS que se ha especificado en el Json, "
						+ "que tiene Nro. Establecimiento: " + this.jsonFactura.getNroestablecimiento() + " y Pto. Emisión: " + this.jsonFactura.getPtoemision() + ", comuniquese con Soporte.";
				return false;
			} else {
				this.bddPostgres.getAuxInvoice().setAdOrgId(adOrgs.get(0));
			}
			
			List<String> cDoctypes = this.bddPostgres.getCDoctypeId("FACTURA", this.connection);
			
			if (cDoctypes.isEmpty()) {
				this.mensaje = "No existe el documento factura en la sucursal en el sistema de facturación electrónica de ATRUMS que se ha especificado en el Json, "
						+ "que tiene Nro. Establecimiento: " + this.jsonFactura.getNroestablecimiento() + " y Pto. Emisión: " + this.jsonFactura.getPtoemision() + ", comuniquese con Soporte.";
				return false;
			} else if (cDoctypes.size() > 1) {
				this.mensaje = "Hay más de un documento factura en la sucursal en el sistema de facturación electrónica de ATRUMS que se ha especificado en el Json, "
						+ "que tiene Nro. Establecimiento: " + this.jsonFactura.getNroestablecimiento() + " y Pto. Emisión: " + this.jsonFactura.getPtoemision() + ", comuniquese con Soporte.";
				return false;
			} else {
				this.bddPostgres.getAuxInvoice().setcDoctypeId(cDoctypes.get(0));
			}
			
			this.bddPostgres.getAuxInvoice().setDocReference(this.jsonFactura.getNrodocumentoref());
			String secuencialRef = this.bddPostgres.getAuxInvoice().getDocReference();
			
			for (int i = 0; i < (9 - this.bddPostgres.getAuxInvoice().getDocReference().length()); i++) {
				secuencialRef = "0" + secuencialRef;
			}
			
			this.bddPostgres.getAuxInvoice().setDocReference(secuencialRef);
			this.jsonFactura.setNrodocumentoref(secuencialRef);
			
			if (this.jsonFactura.getCliente().equals("9999999999999") || 
					this.jsonFactura.getCliente().equals("999999999999") || 
					this.jsonFactura.getCliente().equals("99999999999") || 
					this.jsonFactura.getCliente().equals("9999999999")) {
				this.mensaje = "El ci/ruc es incorrecto del cliente que se ha especificado en el Json.";
				this.connection.rollback();
				return false;
			}
			
			if (this.jsonFactura.getCliente().equals("999999999") ) {
				this.jsonFactura.setCliente("9999999999999");
			}
			
			if (this.jsonFactura.getCliente().length() < 1 || 
					this.jsonFactura.getCliente().length() > 20) {
				this.mensaje = "El ci/ruc es incorrecto del cliente que se ha especificado en el Json.";
				this.connection.rollback();
				return false;
			}
			
			List<String> auxCBpartnerId = this.bddPostgres.getCBpartnerId(this.jsonFactura.getCliente(), this.connection);
			
			if (auxCBpartnerId.isEmpty()) {
				String auxTipoRucCi = null;
				
				if (this.jsonFactura.getCliente().equals("9999999999999")) {
					auxTipoRucCi = "07";
				} else if (this.jsonFactura.getCliente().length() == 10) {
					auxTipoRucCi = "02";
				} else if (this.jsonFactura.getCliente().length() == 13) {
					auxTipoRucCi = "01";
				} else {
					auxTipoRucCi = "03";
				}
				
				String auxNombreCliente = this.jsonFactura.getNombrecliente();
				String auxEmailCliente = this.jsonFactura.getEmailcliente();
				
				this.bddPostgres.getAuxInvoice().setcBpartnerId(this.bddPostgres.putCBpartnerId(
						auxNombreCliente, 
						auxEmailCliente, 
						this.jsonFactura.getCliente(), 
						auxTipoRucCi, 
						this.connection));
				
				if (this.bddPostgres.getAuxInvoice().getcBpartnerId() == null) {
					this.mensaje = "No se guardo el cliente en la bdd del sistema de facturación electrónica de ATRUMS por favor intentelo más tarde, o comuniquese con Soporte.";
					this.connection.rollback();
					return false;
				}
				
				this.bddPostgres.getAuxInvoice().setAdUserId(this.bddPostgres.putAdUserId(auxNombreCliente, auxEmailCliente, "", this.connection));
				
				if (this.bddPostgres.getAuxInvoice().getAdUserId() == null) {
					this.mensaje = "No se guardo el usuario en la bdd del sistema de facturación electrónica de ATRUMS por favor intentelo más tarde, o comuniquese con Soporte.";
					this.connection.rollback();
					return false;
				}
				
				String auxDireccionCliente = this.jsonFactura.getDireccioncliente();
				
				this.bddPostgres.getAuxInvoice().setcLocationId(this.bddPostgres.putCLocationId(auxDireccionCliente, connection));
				
				if (this.bddPostgres.getAuxInvoice().getcLocationId() == null) {
					this.mensaje = "No se guardo la dirección en la bdd del sistema de facturación electrónica de ATRUMS por favor intentelo más tarde, o comuniquese con Soporte.";
					this.connection.rollback();
					return false;
				}
				
				this.bddPostgres.getAuxInvoice().setcBpartnerLocationId(this.bddPostgres.putCBpartnerLocationId(auxDireccionCliente, connection));
				
				if (this.bddPostgres.getAuxInvoice().getcBpartnerLocationId() == null) {
					this.mensaje = "No se guardo la dirección en la bdd del sistema de facturación electrónica de ATRUMS por favor intentelo más tarde, o comuniquese con Soporte.";
					this.connection.rollback();
					return false;
				}
			} else {
				this.bddPostgres.getAuxInvoice().setcBpartnerId(auxCBpartnerId.get(0));
				
				this.bddPostgres.getAuxInvoice().setcBpartnerLocationId(bddPostgres.getCBpartnerLocationId(connection));
				
				if (this.bddPostgres.getAuxInvoice().getcBpartnerLocationId() == null) {
					String auxDireccionCliente = this.jsonFactura.getDireccioncliente();
					
					this.bddPostgres.getAuxInvoice().setcLocationId(bddPostgres.putCLocationId(auxDireccionCliente, connection));
					
					if (this.bddPostgres.getAuxInvoice().getcLocationId() == null) {
						this.mensaje = "No se guardo la dirección en la bdd del sistema de facturación electrónica de ATRUMS por favor intentelo más tarde, o comuniquese con Soporte.";
						this.connection.rollback();
						return false;
					}
					
					this.bddPostgres.getAuxInvoice().setcBpartnerLocationId(bddPostgres.putCBpartnerLocationId(auxDireccionCliente, connection));
					
					if (this.bddPostgres.getAuxInvoice().getcBpartnerLocationId() == null) {
						this.mensaje = "No se guardo la dirección en la bdd del sistema de facturación electrónica de ATRUMS por favor intentelo más tarde, o comuniquese con Soporte.";
						this.connection.rollback();
						return false;
					}
				}
			}
			
			this.bddPostgres.getCInvoice(this.connection);
			
			if (this.bddPostgres.getAuxInvoice().getcInvoiceId() == null) {
				List<Linea> lineas = this.jsonFactura.getLineas();
				
				for(int i=0; i<lineas.size(); i++){
					String auxTipoImpuesto = "";
					
					if (lineas.get(i).getTipoimpuesto().equals("IVA")) {
						auxTipoImpuesto = "2";
					} else if (lineas.get(i).getTipoimpuesto().equals("ICE")) {
						auxTipoImpuesto = "3";
					} else if (lineas.get(i).getTipoimpuesto().equals("IRBPNR")) {
						auxTipoImpuesto = "5";
					} else {
						auxTipoImpuesto = lineas.get(i).getTipoimpuesto();
					}
					
					String auxNombreImpuesto = "";
					
					if (lineas.get(i).getPorcentajeimpuesto().equals("14") && auxTipoImpuesto.equals("2")) {
						auxNombreImpuesto = "IVA 14%";
					} else if (lineas.get(i).getPorcentajeimpuesto().equals("12") && auxTipoImpuesto.equals("2")) {
						auxNombreImpuesto = "IVA 12%";
					} else if (lineas.get(i).getPorcentajeimpuesto().equals("0") && auxTipoImpuesto.equals("2")) {
						auxNombreImpuesto = "IVA 0%";
					} else if (auxTipoImpuesto.equals("2")) {
						auxNombreImpuesto = "IVA " + lineas.get(i).getPorcentajeimpuesto() + "%";
					} else if (lineas.get(i).getPorcentajeimpuesto().equals("15") && auxTipoImpuesto.equals("3")) {
						auxNombreImpuesto = "ICE 15%";
					} else if (auxTipoImpuesto.equals("3")) {
						auxNombreImpuesto = "ICE " + lineas.get(i).getPorcentajeimpuesto() + "%";
					} else if (auxTipoImpuesto.equals("5")) {
						auxNombreImpuesto = "IRBPNR " + lineas.get(i).getPorcentajeimpuesto() + "%";
					} else {
						auxNombreImpuesto = "EXENTO";
					}
					
					List<String> auxCTaxId = this.bddPostgres.getCTaxId(auxNombreImpuesto, lineas.get(i).getCodigoimpuesto(), this.connection);
					
					if (auxCTaxId.isEmpty()) {
						this.mensaje = "No hay el impuesto " + auxNombreImpuesto + " con codigo " + lineas.get(i).getCodigoimpuesto()
								+ " en la bdd del sistema de facturación electrónica de ATRUMS por favor comuniquese con Soporte.";
						connection.rollback();
						return false;
					} else if (auxCTaxId.size() != 1) {
						this.mensaje = "Hay más de un impuesto " + auxNombreImpuesto 
								+ " en la bdd del sistema de facturación electrónica de ATRUMS por favor comuniquese con Soporte.";
						connection.rollback();
						return false;
					}
				}
				
				
				log.info("Tomando nro de documento");
				this.bddPostgres.getAuxInvoice().setDocumentnoOpen(this.bddPostgres.getSequence(this.connection));
				
				String secuencial = this.bddPostgres.getAuxInvoice().getDocumentnoOpen();
				for (int i = 0; i < (9 - this.bddPostgres.getAuxInvoice().getDocumentnoOpen().length()); i++) {
					secuencial = "0" + secuencial;
				}
				
				log.info("Cargando datos de XML");
				this.bddPostgres.getAuxInvoice().setDocumentnoOpen(secuencial);
				this.jsonFactura.setNrodocumentoopen(secuencial);
				
				DateFormat formatori = new SimpleDateFormat("yyyy-MM-dd");
				DateFormat formatmod = new SimpleDateFormat("dd/MM/yyyy");
				
				Date auxDate = formatori.parse(this.jsonFactura.getFechadocumento());
				
				this.bddPostgres.getAuxInvoice().setDateinvoiced(formatmod.format(auxDate));
				
				this.bddPostgres.getAuxInvoice().setTotal(this.jsonFactura.getTotaldocumento());
				
				this.bddPostgres.getAuxInvoice().setServicio(Double.valueOf(this.jsonFactura.getPropina()));
				
				this.bddPostgres.getAuxInvoice().setClaveacceso("");
				
				this.bddPostgres.getAuxInvoice().setAutorization("");
			
				this.bddPostgres.getAuxInvoice().setDocXML("");
				
				this.bddPostgres.getAuxInvoice().setcInvoiceId(this.bddPostgres.putCInvoiceId(this.connection));
				
				if (this.bddPostgres.getAuxInvoice().getcInvoiceId() == null) {
					this.mensaje = "No se guardo el documento en la bdd del sistema de facturación electrónica de ATRUMS por favor intentelo más tarde, o comuniquese con Soporte.";
					this.connection.rollback();
					return false;
				}
			} else {
				
				if (this.bddPostgres.getAuxInvoice().getDocstatus().equals("AP")) {
					this.jsonFactura.setClaveacceso(this.bddPostgres.getAuxInvoice().getClaveacceso());
					this.jsonFactura.setFechaautoizacion(this.bddPostgres.getAuxInvoice().getDateinvoiced());
					this.jsonFactura.setAutorizacion(this.bddPostgres.getAuxInvoice().getAutorization());
					this.jsonFactura.setFechaautoizacion(this.bddPostgres.getAuxInvoice().getFechaAutorizacion());
					
					this.mensaje = "El N° de referencia que desea procesar, ya se encuentra registrado en el sistema. (Nro. de documento: " 
					+ this.bddPostgres.getAuxInvoice().getDocumentnoOpen() 
					+ ", Nro. de referencia: " + this.jsonFactura.getNrodocumentoref();
					
					return false;
				} else if (this.bddPostgres.getAuxInvoice().getDocstatus().equals("RZ")) {
					this.jsonFactura.setClaveacceso(this.bddPostgres.getAuxInvoice().getClaveacceso());
					this.jsonFactura.setFechaautoizacion(this.bddPostgres.getAuxInvoice().getDateinvoiced());
					this.jsonFactura.setAutorizacion(this.bddPostgres.getAuxInvoice().getAutorization());
					this.jsonFactura.setFechaautoizacion(this.bddPostgres.getAuxInvoice().getFechaAutorizacion());
					
					this.mensaje = "El N° de referencia que desea procesar, ya se encuentra registrado en el sistema. (Nro. de documento: "
					+ this.bddPostgres.getAuxInvoice().getDocumentnoOpen() 
					+ ", Nro. de referencia: " + this.jsonFactura.getNrodocumentoref() + ", comuniquese con Soporte, con el nro de documento";
					
					return false;
				} else {
					this.jsonFactura.setClaveacceso(this.bddPostgres.getAuxInvoice().getClaveacceso());
					this.jsonFactura.setFechaautoizacion(this.bddPostgres.getAuxInvoice().getDateinvoiced());
					this.jsonFactura.setAutorizacion(this.bddPostgres.getAuxInvoice().getAutorization());
					this.jsonFactura.setFechaautoizacion(this.bddPostgres.getAuxInvoice().getFechaAutorizacion());
					
					this.mensaje = "El N° de referencia que desea procesar, ya se encuentra registrado en el sistema. (Nro. de documento: " 
					+ this.bddPostgres.getAuxInvoice().getDocumentnoOpen() 
					+ ", Nro. de referencia: " + this.jsonFactura.getNrodocumentoref();
					
					return false;
				}
				
				/*this.jsonFactura.setNrodocumentoopen(this.bddPostgres.getAuxInvoice().getDocumentnoOpen());
				this.bddPostgres.getAuxInvoice().setTotal(this.jsonFactura.getTotaldocumento());
				this.bddPostgres.getAuxInvoice().setServicio(Double.valueOf(this.jsonFactura.getPropina()));*/
			} 
			
			/****************************Cambio a Crear Borrador Fin*****************************************************/
			
			GenerarXMLFactura xmlFactura = new GenerarXMLFactura(this.jsonFactura, this.connection);
			
			List<Client> infoTributaria = this.bddPostgres.getInfoTributariaAdClient(this.jsonFactura.getNombreempresa(), this.jsonFactura.getNroestablecimiento(), this.jsonFactura.getPtoemision(), connection);
			
			if (infoTributaria.isEmpty()) {
				this.mensaje = "No existe la empresa en el sistema de facturación electrónica de ATRUMS que se ha especificado en el Json, comuniquese con Soporte.";
				return false;
			} else if (infoTributaria.size() > 1) {
				this.mensaje = "Hay mas de una empresa en el sistema de facturación electrónica que se ha especificado con el mismo nombre en el Json, comuniquese con Soporte.";
				return false;
			} else {
				xmlFactura.setInfoTributaria(infoTributaria.get(0));
			}
			
			xml = xmlFactura.generarXML();
			
			if (xml == null) {
				this.mensaje = xmlFactura.getMensaje();
				return false;
			} else { 								
				this.jsonFactura.setClaveacceso(xmlFactura.getClaveacceso());
				
			    //this.connection.commit();
				
				this.jsonFactura.setFechaautoizacion("");
				this.jsonFactura.setAutorizacion(this.jsonFactura.getClaveacceso());
				this.docxml = null;
				this.filexml = null;
				
				return true;
				
				/*SRIDocumentoAutorizado autorizadoPre = new SRIDocumentoAutorizado();
				SRIDocumentoRecibido recibido = new SRIDocumentoRecibido();
				
				ServiceAutorizacion serviceAutorizacionPre = new ServiceAutorizacion(confService, 
						infoTributaria.get(0).getAmbiente(), 
						xmlFactura.getClaveacceso());
				autorizadoPre = serviceAutorizacionPre.CallAutorizado();
				
				if (!autorizadoPre.getEstadoespecifico().equals("AUT")) {
					ServiceRecibido serviceRecibido = new ServiceRecibido(confService, 
							infoTributaria.get(0).getAmbiente(), xml);
					recibido = serviceRecibido.CallRecibido();
				}else{
					recibido.setEstado("RECIBIDO");
					recibido.setEstadoespecifico("REC");
				}
				
				if (recibido.getMensaje() != null) {
					if (recibido.getMensaje().indexOf("CLAVE ACCESO REGISTRADA") != -1) {
						recibido.setEstadoespecifico("DEV");
						recibido.setMensaje(recibido.getMensaje());
					} else {
						this.mensaje = recibido.getMensaje();
					}
				}
				
				if (recibido.getInformacion() != null) {
					this.mensaje = recibido.getMensaje() + " - " + recibido.getInformacion().replace("'", "");
				}
				
				if (recibido.getEstadoespecifico().equals("REC")) {
					SRIDocumentoAutorizado autorizado = new SRIDocumentoAutorizado();
					ServiceAutorizacion serviceAutorizacion = new ServiceAutorizacion(confService, 
							infoTributaria.get(0).getAmbiente(), 
							xmlFactura.getClaveacceso());
					autorizado = serviceAutorizacion.CallAutorizado();
					
					if (autorizado.getEstadoespecifico().equals("AUT")) {
						if (autorizado.getInformacion() != null) {
							this.mensaje = autorizado.getMensaje() + " - " + autorizado.getInformacion().replace("'", "");
						}
						
						this.jsonFactura.setFechaautoizacion(autorizado.getFechaAutorizacion());
						this.jsonFactura.setAutorizacion(autorizado.getNumeroAutorizacion());
						this.docxml = autorizado.getDocXML();
						this.filexml = autorizado.getDocFile();
						
						return true;
					} else {
						if (autorizado.getInformacion() != null) {
							this.mensaje = autorizado.getMensaje() + " - " + autorizado.getInformacion().replace("'", "");
						}
						this.bddPostgres.updateInvoiceMensaje(this.mensaje, this.connection);
						this.connection.commit();
						
						return false;
					}
				} else {
					this.bddPostgres.updateInvoiceMensaje(this.mensaje, this.connection);
					this.connection.commit();
					
					return false;
				}*/
			}
		} catch (Exception ex) {
			// TODO: handle exception
			StringWriter stack = new StringWriter();
			ex.printStackTrace(new PrintWriter(stack));
			log.warn("Caught exception; decorating with appropriate status template : " + stack.toString());
		}
		
		return false;
	}
	
	public boolean procesarNotaSRI() {
		//ConfService confService = new ConfService();
		String xml = null;
		
		try {
			
			if (!this.jsonNotaCredito.getTipodoc().equals("04")) {
				this.mensaje = "No es una nota de crédito el tipo de documento que se ha especificado en el Json.";
				return false;
			}
			
			if (this.connection == null) {
				this.mensaje = "Hay fallas con la bdd del sistema de facturación electrónica de ATRUMS por favor intentelo más tarde, o comuniquese con Soporte.";
				return false;
			}
			
			/****************************Cambio a Crear Borrador Inicio*****************************************************/
			
			List<String> adClients = this.bddPostgres.getAdClientId(this.jsonNotaCredito.getNombreempresa(), connection);
			
			if (adClients.isEmpty()) {
				this.mensaje = "No existe la empresa en el sistema de facturación electrónica de ATRUMS que se ha especificado en el Json, comuniquese con Soporte.";
				return false;
			} else if (adClients.size() > 1) {
				this.mensaje = "Hay más de una empresa en el sistema de facturación electrónica que se ha especificado con el mismo nombre en el Json, comuniquese con Soporte.";
				return false;
			} else {
				this.docAutInvoice.setAdClientId(adClients.get(0));
				this.docAutInvoice.setSucursal(this.jsonNotaCredito.getNroestablecimiento());
				this.docAutInvoice.setPtoemision(this.jsonNotaCredito.getPtoemision());
				this.docAutInvoice.setNroRelacionado(this.jsonNotaCredito.getNumDocRelacionado().replaceAll("-", ""));
				
				this.bddPostgres.setAuxInvoice(this.docAutInvoice);
			}
			
			List<String> adCinvoices = this.bddPostgres.getcInvoiceRelacionadoId(connection);
			
			if (adCinvoices.isEmpty()) {
				this.mensaje = "No existe la factura original en el sistema de facturación electrónica de ATRUMS que se ha especificado en el Json, "
						+ "que tiene Nro. Documento - Original: " + this.jsonNotaCredito.getNumDocRelacionado() + ", comuniquese con Soporte.";
				return false;
			} else if (adCinvoices.size() > 1) {
				this.mensaje = "Hay más de una factura que se relaciona con la factura original en el sistema de facturación electrónica de ATRUMS que se ha especificado en el Json, "
						+ "que tiene Nro. Documento - Original: " + this.jsonNotaCredito.getNumDocRelacionado() + ", comuniquese con Soporte.";
				return false;
			} else {
				this.bddPostgres.getAuxInvoice().setcInvoiceIdRelacionado(adCinvoices.get(0));
				this.bddPostgres.getAuxInvoice().setDescription(this.jsonNotaCredito.getMotivo());
			}
			
			List<String> adOrgs = this.bddPostgres.getAdOrgId(connection);
			
			if (adOrgs.isEmpty()) {
				this.mensaje = "No existe la sucursal en el sistema de facturación electrónica de ATRUMS que se ha especificado en el Json, "
						+ "que tiene Nro. Establecimiento: " + this.jsonNotaCredito.getNroestablecimiento() + " y Pto. Emisión: " + this.jsonNotaCredito.getPtoemision() + ", comuniquese con Soporte.";
				return false;
			} else if (adOrgs.size() > 1) {
				this.mensaje = "Hay más de una sucursal en el sistema de facturación electrónica de ATRUMS que se ha especificado en el Json, "
						+ "que tiene Nro. Establecimiento: " + this.jsonNotaCredito.getNroestablecimiento() + " y Pto. Emisión: " + this.jsonNotaCredito.getPtoemision() + ", comuniquese con Soporte.";
				return false;
			} else {
				this.bddPostgres.getAuxInvoice().setAdOrgId(adOrgs.get(0));
			}
			
			List<String> cDoctypes = this.bddPostgres.getCDoctypeId("NOTA", connection);
			
			if (cDoctypes.isEmpty()) {
				this.mensaje = "No existe el documento factura en la sucursal en el sistema de facturación electrónica de ATRUMS que se ha especificado en el Json, "
						+ "que tiene Nro. Establecimiento: " + this.jsonNotaCredito.getNroestablecimiento() + " y Pto. Emisión: " + this.jsonNotaCredito.getPtoemision() + ", comuniquese con Soporte.";
				return false;
			} else if (cDoctypes.size() > 1) {
				this.mensaje = "Hay más de un documento factura en la sucursal en el sistema de facturación electrónica de ATRUMS que se ha especificado en el Json, "
						+ "que tiene Nro. Establecimiento: " + this.jsonNotaCredito.getNroestablecimiento() + " y Pto. Emisión: " + this.jsonNotaCredito.getPtoemision() + ", comuniquese con Soporte.";
				return false;
			} else {
				this.bddPostgres.getAuxInvoice().setcDoctypeId(cDoctypes.get(0));
			}
			
			this.bddPostgres.getAuxInvoice().setDocReference(this.jsonNotaCredito.getNrodocumentoref());
			String secuencialRef = this.bddPostgres.getAuxInvoice().getDocReference();
			
			for (int i = 0; i < (9 - this.bddPostgres.getAuxInvoice().getDocReference().length()); i++) {
				secuencialRef = "0" + secuencialRef;
			}
			
			this.bddPostgres.getAuxInvoice().setDocReference(secuencialRef);
			this.jsonNotaCredito.setNrodocumentoref(secuencialRef);
			
			if (this.jsonNotaCredito.getCliente().equals("9999999999999") || 
					this.jsonNotaCredito.getCliente().equals("999999999999") || 
					this.jsonNotaCredito.getCliente().equals("99999999999") || 
					this.jsonNotaCredito.getCliente().equals("9999999999")) {
				this.mensaje = "El ci/ruc es incorrecto del cliente que se ha especificado en el Json.";
				this.connection.rollback();
				return false;
			}
			
			if (this.jsonNotaCredito.getCliente().equals("999999999") ) {
				this.jsonNotaCredito.setCliente("9999999999999");
			}
			
			if (this.jsonNotaCredito.getCliente().length() < 1 || 
					this.jsonNotaCredito.getCliente().length() > 20) {
				this.mensaje = "El ci/ruc es incorrecto del cliente que se ha especificado en el Json.";
				this.connection.rollback();
				return false;
			}
			
			List<String> auxCBpartnerId = this.bddPostgres.getCBpartnerId(this.jsonNotaCredito.getCliente(), connection);
			
			if (auxCBpartnerId.isEmpty()) {
				String auxTipoRucCi = null;
				
				if (this.jsonNotaCredito.getCliente().equals("9999999999999")) {
					auxTipoRucCi = "07";
				} else if (this.jsonNotaCredito.getCliente().length() == 10) {
					auxTipoRucCi = "02";
				} else if (this.jsonNotaCredito.getCliente().length() == 13) {
					auxTipoRucCi = "01";
				} else {
					auxTipoRucCi = "03";
				}
				
				String auxNombreCliente = this.jsonNotaCredito.getNombrecliente();
				String auxEmailCliente = this.jsonNotaCredito.getEmailcliente();
				
				this.bddPostgres.getAuxInvoice().setcBpartnerId(this.bddPostgres.putCBpartnerId(
						auxNombreCliente, 
						auxEmailCliente, 
						this.jsonNotaCredito.getCliente(), 
						auxTipoRucCi, 
						connection));
				
				if (this.bddPostgres.getAuxInvoice().getcBpartnerId() == null) {
					this.mensaje = "No se guardo el cliente en la bdd del sistema de facturación electrónica de ATRUMS por favor intentelo más tarde, o comuniquese con Soporte.";
					connection.rollback();
					return false;
				}
				
				this.bddPostgres.getAuxInvoice().setAdUserId(this.bddPostgres.putAdUserId(auxNombreCliente, auxEmailCliente, "", connection));
				
				if (this.bddPostgres.getAuxInvoice().getAdUserId() == null) {
					this.mensaje = "No se guardo el usuario en la bdd del sistema de facturación electrónica de ATRUMS por favor intentelo más tarde, o comuniquese con Soporte.";
					connection.rollback();
					return false;
				}
				
				String auxDireccionCliente = this.jsonNotaCredito.getDireccioncliente();
				
				this.bddPostgres.getAuxInvoice().setcLocationId(this.bddPostgres.putCLocationId(auxDireccionCliente, connection));
				
				if (this.bddPostgres.getAuxInvoice().getcLocationId() == null) {
					this.mensaje = "No se guardo la dirección en la bdd del sistema de facturación electrónica de ATRUMS por favor intentelo más tarde, o comuniquese con Soporte.";
					connection.rollback();
					return false;
				}
				
				this.bddPostgres.getAuxInvoice().setcBpartnerLocationId(this.bddPostgres.putCBpartnerLocationId(auxDireccionCliente, connection));
				
				if (this.bddPostgres.getAuxInvoice().getcBpartnerLocationId() == null) {
					this.mensaje = "No se guardo la dirección en la bdd del sistema de facturación electrónica de ATRUMS por favor intentelo más tarde, o comuniquese con Soporte.";
					connection.rollback();
					return false;
				}
			} else {
				this.bddPostgres.getAuxInvoice().setcBpartnerId(auxCBpartnerId.get(0));
				
				this.bddPostgres.getAuxInvoice().setcBpartnerLocationId(bddPostgres.getCBpartnerLocationId(connection));
				
				if (this.bddPostgres.getAuxInvoice().getcBpartnerLocationId() == null) {
					String auxDireccionCliente = this.jsonNotaCredito.getDireccioncliente();
					
					this.bddPostgres.getAuxInvoice().setcLocationId(bddPostgres.putCLocationId(auxDireccionCliente, connection));
					
					if (this.bddPostgres.getAuxInvoice().getcLocationId() == null) {
						this.mensaje = "No se guardo la dirección en la bdd del sistema de facturación electrónica de ATRUMS por favor intentelo más tarde, o comuniquese con Soporte.";
						connection.rollback();
						return false;
					}
					
					this.bddPostgres.getAuxInvoice().setcBpartnerLocationId(bddPostgres.putCBpartnerLocationId(auxDireccionCliente, connection));
					
					if (this.bddPostgres.getAuxInvoice().getcBpartnerLocationId() == null) {
						this.mensaje = "No se guardo la dirección en la bdd del sistema de facturación electrónica de ATRUMS por favor intentelo más tarde, o comuniquese con Soporte.";
						connection.rollback();
						return false;
					}
				}
			}
			
			this.bddPostgres.getCInvoice(this.connection);
			
			if (this.bddPostgres.getAuxInvoice().getcInvoiceId() == null) {
				List<Linea> lineas = this.jsonNotaCredito.getLineas();
				
				for(int i=0; i<lineas.size(); i++){
					String auxTipoImpuesto = "";
					
					if (lineas.get(i).getTipoimpuesto().equals("IVA")) {
						auxTipoImpuesto = "2";
					} else if (lineas.get(i).getTipoimpuesto().equals("ICE")) {
						auxTipoImpuesto = "3";
					} else if (lineas.get(i).getTipoimpuesto().equals("IRBPNR")) {
						auxTipoImpuesto = "5";
					} else {
						auxTipoImpuesto = lineas.get(i).getTipoimpuesto();
					}
					
					String auxNombreImpuesto = "";
					
					if (lineas.get(i).getPorcentajeimpuesto().equals("14") && auxTipoImpuesto.equals("2")) {
						auxNombreImpuesto = "IVA 14%";
					} else if (lineas.get(i).getPorcentajeimpuesto().equals("12") && auxTipoImpuesto.equals("2")) {
						auxNombreImpuesto = "IVA 12%";
					} else if (lineas.get(i).getPorcentajeimpuesto().equals("0") && auxTipoImpuesto.equals("2")) {
						auxNombreImpuesto = "IVA 0%";
					} else if (auxTipoImpuesto.equals("2")) {
						auxNombreImpuesto = "IVA " + lineas.get(i).getPorcentajeimpuesto() + "%";
					} else if (lineas.get(i).getPorcentajeimpuesto().equals("15") && auxTipoImpuesto.equals("3")) {
						auxNombreImpuesto = "ICE 15%";
					} else if (auxTipoImpuesto.equals("3")) {
						auxNombreImpuesto = "ICE " + lineas.get(i).getPorcentajeimpuesto() + "%";
					} else if (auxTipoImpuesto.equals("5")) {
						auxNombreImpuesto = "IRBPNR " + lineas.get(i).getPorcentajeimpuesto() + "%";
					} else {
						auxNombreImpuesto = "EXENTO";
					}
					
					List<String> auxCTaxId = this.bddPostgres.getCTaxId(auxNombreImpuesto, lineas.get(i).getCodigoimpuesto(), this.connection);
					
					if (auxCTaxId.isEmpty()) {
						this.mensaje = "No hay el impuesto " + auxNombreImpuesto + " con codigo " + lineas.get(i).getCodigoimpuesto()
								+ " en la bdd del sistema de facturación electrónica de ATRUMS por favor comuniquese con Soporte.";
						connection.rollback();
						return false;
					} else if (auxCTaxId.size() != 1) {
						this.mensaje = "Hay más de un impuesto " + auxNombreImpuesto 
								+ " en la bdd del sistema de facturación electrónica de ATRUMS por favor comuniquese con Soporte.";
						connection.rollback();
						return false;
					}
				}
				
				this.bddPostgres.getAuxInvoice().setDocumentnoOpen(this.bddPostgres.getSequence(this.connection));
				
				String secuencial = this.bddPostgres.getAuxInvoice().getDocumentnoOpen();
				for (int i = 0; i < (9 - this.bddPostgres.getAuxInvoice().getDocumentnoOpen().length()); i++) {
					secuencial = "0" + secuencial;
				}
				
				this.bddPostgres.getAuxInvoice().setDocumentnoOpen(secuencial);
				this.jsonNotaCredito.setNrodocumentoopen(secuencial);
				
				DateFormat formatori = new SimpleDateFormat("yyyy-MM-dd");
				DateFormat formatmod = new SimpleDateFormat("dd/MM/yyyy");
				
				Date auxDate = formatori.parse(this.jsonNotaCredito.getFechadocumento());
				
				this.bddPostgres.getAuxInvoice().setDateinvoiced(formatmod.format(auxDate));
				
				this.bddPostgres.getAuxInvoice().setTotal(this.jsonNotaCredito.getTotaldocumento());
				
				this.bddPostgres.getAuxInvoice().setClaveacceso("");
				
				this.bddPostgres.getAuxInvoice().setAutorization("");
				
				this.bddPostgres.getAuxInvoice().setDocXML("");
				
				this.bddPostgres.getAuxInvoice().setcInvoiceId(this.bddPostgres.putCInvoiceNotaId(this.connection));
				
				if (this.bddPostgres.getAuxInvoice().getcInvoiceId() == null) {
					this.mensaje = "No se guardo el documento en la bdd del sistema de facturación electrónica de ATRUMS por favor intentelo más tarde, o comuniquese con Soporte.";
					connection.rollback();
					return false;
				}
			} else {
				
				if (this.bddPostgres.getAuxInvoice().getDocstatus().equals("AP")) {
					this.jsonNotaCredito.setClaveacceso(this.bddPostgres.getAuxInvoice().getClaveacceso());
					this.jsonNotaCredito.setFechaautoizacion(this.bddPostgres.getAuxInvoice().getDateinvoiced());
					this.jsonNotaCredito.setAutorizacion(this.bddPostgres.getAuxInvoice().getAutorization());
					this.jsonNotaCredito.setFechaautoizacion(this.bddPostgres.getAuxInvoice().getFechaAutorizacion());
					
					this.mensaje = "El N° de referencia que desea procesar, ya se encuentra registrado en el sistema. (Nro. de documento: " 
					+ this.bddPostgres.getAuxInvoice().getDocumentnoOpen() 
					+ ", Nro. referencia: " + this.jsonNotaCredito.getNrodocumentoref();
					
					return false;
				} else if (this.bddPostgres.getAuxInvoice().getDocstatus().equals("RZ")) {
					this.jsonNotaCredito.setClaveacceso(this.bddPostgres.getAuxInvoice().getClaveacceso());
					this.jsonNotaCredito.setFechaautoizacion(this.bddPostgres.getAuxInvoice().getDateinvoiced());
					this.jsonNotaCredito.setAutorizacion(this.bddPostgres.getAuxInvoice().getAutorization());
					this.jsonNotaCredito.setFechaautoizacion(this.bddPostgres.getAuxInvoice().getFechaAutorizacion());
					
					this.mensaje = "El N° de referencia que desea procesar, ya se encuentra registrado en el sistema. (Nro. de documento: "
					+ this.bddPostgres.getAuxInvoice().getDocumentnoOpen() 
					+ ", Nro. referencia: " + this.jsonNotaCredito.getNrodocumentoref();
					
					return false;
				} else {
					this.jsonNotaCredito.setClaveacceso(this.bddPostgres.getAuxInvoice().getClaveacceso());
					this.jsonNotaCredito.setFechaautoizacion(this.bddPostgres.getAuxInvoice().getDateinvoiced());
					this.jsonNotaCredito.setAutorizacion(this.bddPostgres.getAuxInvoice().getAutorization());
					this.jsonNotaCredito.setFechaautoizacion(this.bddPostgres.getAuxInvoice().getFechaAutorizacion());
					
					this.mensaje = "El N° de referencia que desea procesar, ya se encuentra registrado en el sistema. (Nro. de documento: " 
					+ this.bddPostgres.getAuxInvoice().getDocumentnoOpen() 
					+ ", Nro. referencia: " + this.jsonNotaCredito.getNrodocumentoref();
					
					return false;
				}
				
				/*this.jsonNotaCredito.setNrodocumentoopen(this.bddPostgres.getAuxInvoice().getDocumentnoOpen());
				this.bddPostgres.getAuxInvoice().setTotal(this.jsonNotaCredito.getTotaldocumento());*/
			}
			
			/****************************Cambio a Crear Borrador Fin*****************************************************/
			
			GenerarXMLNotaCredito xmlNota = new GenerarXMLNotaCredito(this.jsonNotaCredito, this.connection);
			
			List<Client> infoTributaria = this.bddPostgres.getInfoTributariaAdClient(this.jsonNotaCredito.getNombreempresa(), this.jsonNotaCredito.getNroestablecimiento(), this.jsonNotaCredito.getPtoemision(), connection);
			
			if (infoTributaria.isEmpty()) {
				this.mensaje = "No existe la empresa en el sistema de facturación electrónica de ATRUMS que se ha especificado en el Json, comuniquese con Soporte.";
				return false;
			} else if (infoTributaria.size() > 1) {
				this.mensaje = "Hay mas de una empresa en el sistema de facturación electrónica que se ha especificado con el mismo nombre en el Json, comuniquese con Soporte.";
				return false;
			} else {
				xmlNota.setInfoTributaria(infoTributaria.get(0));
			}
			
			xml = xmlNota.generarXML();
			
			if (xml == null) {
				this.mensaje = xmlNota.getMensaje();
				return false;
			} else { 
				this.jsonNotaCredito.setClaveacceso(xmlNota.getClaveacceso());
				
				//this.connection.commit();
				
				this.jsonNotaCredito.setFechaautoizacion("");
				this.jsonNotaCredito.setAutorizacion(this.jsonNotaCredito.getClaveacceso());
				this.docxml = null;
				this.filexml = null;
				
				return true;
				/*SRIDocumentoAutorizado autorizadoPre = new SRIDocumentoAutorizado();
				SRIDocumentoRecibido recibido = new SRIDocumentoRecibido();
				
				ServiceAutorizacion serviceAutorizacionPre = new ServiceAutorizacion(confService, 
						infoTributaria.get(0).getAmbiente(), 
						xmlNota.getClaveacceso());
				autorizadoPre = serviceAutorizacionPre.CallAutorizado();
				
				if (!autorizadoPre.getEstadoespecifico().equals("AUT")) {
					ServiceRecibido serviceRecibido = new ServiceRecibido(confService, 
							infoTributaria.get(0).getAmbiente(), xml);
					recibido = serviceRecibido.CallRecibido();
					
					this.connection.commit();
				}else{
					recibido.setEstado("RECIBIDO");
					recibido.setEstadoespecifico("REC");
				}
				
				if (recibido.getMensaje() != null) {
					if (recibido.getMensaje().indexOf("CLAVE ACCESO REGISTRADA") != -1) {
						recibido.setEstadoespecifico("DEV");
						recibido.setMensaje(recibido.getMensaje());
					} else {
						this.mensaje = recibido.getMensaje();
					}
				}
				
				if (recibido.getInformacion() != null) {
					this.mensaje = recibido.getMensaje() + " - " + recibido.getInformacion().replace("'", "");
				}
				
				if (recibido.getEstadoespecifico().equals("REC")) {
					SRIDocumentoAutorizado autorizado = new SRIDocumentoAutorizado();
					ServiceAutorizacion serviceAutorizacion = new ServiceAutorizacion(confService, 
							infoTributaria.get(0).getAmbiente(), 
							xmlNota.getClaveacceso());
					autorizado = serviceAutorizacion.CallAutorizado();
					
					if (autorizado.getEstadoespecifico().equals("AUT")) {
						if (autorizado.getInformacion() != null) {
							this.mensaje = autorizado.getMensaje() + " - " + autorizado.getInformacion().replace("'", "");
						}
						
						this.jsonNotaCredito.setFechaautoizacion(autorizado.getFechaAutorizacion());
						this.jsonNotaCredito.setAutorizacion(autorizado.getNumeroAutorizacion());
						this.docxml = autorizado.getDocXML();
						this.filexml = autorizado.getDocFile();
						
						return true;
					} else {
						if (autorizado.getInformacion() != null) {
							this.mensaje = autorizado.getMensaje() + " - " + autorizado.getInformacion().replace("'", "");
						}
						
						this.bddPostgres.updateInvoiceMensaje(this.mensaje, this.connection);
						this.connection.commit();
						
						return false;
					}
				} else {
					this.bddPostgres.updateInvoiceMensaje(this.mensaje, this.connection);
					this.connection.commit();
					
					return false;
				}*/
			}
		} catch (Exception ex) {
			// TODO: handle exception
			StringWriter stack = new StringWriter();
			ex.printStackTrace(new PrintWriter(stack));
			log.warn("Caught exception; decorating with appropriate status template : " + stack.toString());
		} /*finally {
			try {if (connection != null) connection.close();} catch (SQLException ex) {}
		}*/
		
		return false;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
}
