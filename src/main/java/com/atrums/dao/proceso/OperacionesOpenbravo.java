package com.atrums.dao.proceso;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;

//import javax.sql.DataSource;

import org.apache.log4j.Logger;

//import com.atrums.dao.operaciones.OperacionesEnvioEmail;
import com.atrums.modelo.Client;
import com.atrums.modelo.Configuracion;
import com.atrums.modelo.DocumentoLinea;
import com.atrums.modelo.Invoice;
import com.atrums.modelo.InvoiceLine;
import com.atrums.modelo.JsonFactura;
import com.atrums.modelo.JsonNotaCredito;
import com.atrums.modelo.Linea;
import com.atrums.modelo.Pago;
//import com.atrums.modelo.PocEmail;
//import com.atrums.persistencia.Conexion;
import com.atrums.persistencia.OperacionesBDDPostgres;

public class OperacionesOpenbravo {
	static final Logger log = Logger.getLogger(OperacionesOpenbravo.class);
	private JsonFactura jsonFactura = null;
	private JsonNotaCredito jsonNotaCredito = null;
	private String mensaje = null;
	private String docxml = null;
	private File filexml = null;
	private OperacionesBDDPostgres bddPostgres = new OperacionesBDDPostgres();
	private Configuracion configuracion = new Configuracion();
	private Connection connection = null;
	//private DataSource dataSource = Conexion.getDataSource();
	private Invoice docAutInvoice = null;

	public OperacionesOpenbravo(JsonFactura jsonFactura, 
			String docxml, 
			File filexml, 
			Invoice docAutInvoice, 
			Connection connection) {
		this.jsonFactura = jsonFactura;
		this.docxml = docxml;
		this.docAutInvoice = docAutInvoice;
		this.filexml = filexml;		
		this.connection = connection;
	}
	
	public OperacionesOpenbravo(JsonNotaCredito jsonNotaCredito, 
			String docxml, 
			File filexml, 
			Invoice docAutInvoice, 
			Connection connection) {
		this.jsonNotaCredito = jsonNotaCredito;
		this.docxml = docxml;
		this.docAutInvoice = docAutInvoice;
		this.filexml = filexml;
		this.connection = connection;
	}
	
	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public boolean procesarOpenbravo() {
		List<InvoiceLine> auxInvoiceLineas = new ArrayList<InvoiceLine>();
		
		try {
			if (this.connection == null) {
				this.mensaje = "Hay fallas con la bdd del sistema de facturaci�n electr�nica de ATRUMS por favor intentelo m�s tarde, o comuniquese con Soporte.";
				return false;
			}
			
			this.bddPostgres.setAuxInvoice(this.docAutInvoice);
			
			List<Client> infoTributaria = this.bddPostgres.getInfoTributariaAdClient(this.jsonFactura.getNombreempresa(), this.jsonFactura.getNroestablecimiento(), this.jsonFactura.getPtoemision(), connection);
			
			if (infoTributaria.isEmpty()) {
				this.mensaje = "No existe la empresa en el sistema de facturaci�n electr�nica de ATRUMS que se ha especificado en el Json, comuniquese con Soporte.";
				return false;
			} else if (infoTributaria.size() > 1) {
				this.mensaje = "Hay mas de una empresa en el sistema de facturaci�n electr�nica que se ha especificado con el mismo nombre en el Json, comuniquese con Soporte.";
				return false;
			} else {
				this.bddPostgres.getAuxInvoice().setInfoTributaria(infoTributaria.get(0));
			}
			
			this.bddPostgres.getAuxInvoice().setClaveacceso(this.jsonFactura.getClaveacceso());
			
			DateFormat formatori = new SimpleDateFormat("ddMMyyyy");
			DateFormat formatmod = new SimpleDateFormat("dd/MM/yyyy");
			
			Date auxDate = formatori.parse(this.jsonFactura.getClaveacceso().substring(0, 8));
			
			this.bddPostgres.getAuxInvoice().setDateinvoiced(formatmod.format(auxDate));
			
			this.bddPostgres.getAuxInvoice().setAutorization(this.jsonFactura.getAutorizacion());
			this.bddPostgres.getAuxInvoice().setDocXML(this.docxml);
			this.bddPostgres.getAuxInvoice().setFileXML(this.filexml);
			
			if (!this.bddPostgres.updateInvoice("PD", this.connection)) {
				this.connection.rollback();
				this.mensaje = "Hay fallas con la bdd del sistema de facturaci�n electr�nica de ATRUMS por favor intentelo m�s tarde, o comuniquese con Soporte.";
				return false;
			}
			
			List<Linea> lineas = this.jsonFactura.getLineas();
			
			int linea = 0;
			
			for(int i=0; i<lineas.size(); i++){
				InvoiceLine auxInvoiceLine = new InvoiceLine();
				
				linea = linea + 10;
				auxInvoiceLine.setLinea(linea);
				
				List<String> auxMProductId = this.bddPostgres.getMProductId(lineas.get(i).getCodigoProducto(), connection);
				
				if (auxMProductId.isEmpty()) {
					List<String> auxProductCatergoryId = this.bddPostgres.getMProductCategoryId(this.configuracion.getCategoriaproducto(), connection); 
					
					if (auxProductCatergoryId.isEmpty()) {
						auxInvoiceLine.setmProductCategoryId(bddPostgres.putMProductCategoryId(this.configuracion.getCategoriaproducto(), connection));
						
						if (auxInvoiceLine.getmProductCategoryId() == null) {
							this.mensaje = "No se guardo la categoria del producto c�digo " + this.configuracion.getCategoriaproducto() + " en la bdd del sistema de facturaci�n electr�nica de ATRUMS por favor intentelo m�s tarde, o comuniquese con Soporte.";
							connection.rollback();
							return false;
						}
					} else if (auxProductCatergoryId.size() != 1) {
						this.mensaje = "Hay m�s de una categor�a del producto " + configuracion.getCategoriaproducto() + " en la bdd del sistema de facturaci�n electr�nica de ATRUMS por favor intentelo m�s tarde, o comuniquese con Soporte.";
						connection.rollback();
						return false;
					} else {
						auxInvoiceLine.setmProductCategoryId(auxProductCatergoryId.get(0));
						auxProductCatergoryId = null;
					}
					
					DocumentoLinea auxLinea = new DocumentoLinea();
					auxLinea.setItemcdgodtfc(lineas.get(i).getCodigoProducto());
					auxLinea.setDtfcdscr(lineas.get(i).getNombreProducto().toString().replaceAll("'",""));
					
					auxInvoiceLine.setmProductId(bddPostgres.putMProductId(auxInvoiceLine.getmProductCategoryId(), auxLinea, connection));
					
					if (auxInvoiceLine.getmProductId() == null) {
						this.mensaje = "No se guardo el producto con este c�digo " + lineas.get(i).getCodigoProducto() + " en la bdd del sistema de facturaci�n electr�nica de ATRUMS por favor intentelo m�s tarde, o comuniquese con Soporte.";
						connection.rollback();
						return false;
					}
					
				} else if (auxMProductId.size() != 1) {
					this.mensaje = "Hay m�s de un producto con el mismo c�digo en la bdd del sistema de facturaci�n electr�nica de ATRUMS por favor intentelo m�s tarde, o comuniquese con Soporte.";
					connection.rollback();
					return false;
				} else {
					auxInvoiceLine.setmProductId(auxMProductId.get(0));
					auxMProductId = null;
				}
				
				auxInvoiceLine.setCantidad(lineas.get(i).getCantidad());
				auxInvoiceLine.setPrecioUnitario(lineas.get(i).getPrecio());
				auxInvoiceLine.setDescuento(lineas.get(i).getDescuento());
				
				DecimalFormat format2 = new DecimalFormat("0.00");
				DecimalFormat format3 = new DecimalFormat("0.000");
				double auxPorcentajeDescuento = 0;
				
				if (Double.valueOf(auxInvoiceLine.getPrecioUnitario()) != 0) {
					auxPorcentajeDescuento = Double.valueOf(auxInvoiceLine.getDescuento()) * (100/(Double.valueOf(auxInvoiceLine.getPrecioUnitario()) * Double.valueOf(auxInvoiceLine.getCantidad())));
				} 
				
				auxInvoiceLine.setPorcentajeDescuento(format3.format(auxPorcentajeDescuento).replace(",", "."));
				auxInvoiceLine.setTotalLineaSinImpuestos(lineas.get(i).getTotallinea());
				auxInvoiceLine.setValor(lineas.get(i).getImpuestolinea());
				
				double auxImpuesto = Double.valueOf(auxInvoiceLine.getValor());
				double auxTotal = Double.valueOf(auxInvoiceLine.getTotalLineaSinImpuestos());
				
				auxInvoiceLine.setTotalLinea(format2.format(auxTotal + auxImpuesto).replace(",", "."));
				
				String auxTipoImpuesto = "";
				
				if (lineas.get(i).getTipoimpuesto().equals("IVA")) {
					auxTipoImpuesto = "2";
				} else if (lineas.get(i).getTipoimpuesto().equals("ICE")) {
					auxTipoImpuesto = "3";
				} else if (lineas.get(i).getTipoimpuesto().equals("IRBPNR")) {
					auxTipoImpuesto = "5";
				} else {
					auxTipoImpuesto = lineas.get(i).getTipoimpuesto();
				}
				
				if (lineas.get(i).getPorcentajeimpuesto().equals("14") && auxTipoImpuesto.equals("2")) {
					auxInvoiceLine.setPorcentaje("IVA 14%");
				} else if (lineas.get(i).getPorcentajeimpuesto().equals("12") && auxTipoImpuesto.equals("2")) {
					auxInvoiceLine.setPorcentaje("IVA 12%");
				} else if (lineas.get(i).getPorcentajeimpuesto().equals("0") && auxTipoImpuesto.equals("2")) {
					auxInvoiceLine.setPorcentaje("IVA 0%");
				} else if (auxTipoImpuesto.equals("2")) {
					auxInvoiceLine.setPorcentaje("IVA " + lineas.get(i).getPorcentajeimpuesto() + "%");
				} else if (lineas.get(i).getPorcentajeimpuesto().equals("15") && auxTipoImpuesto.equals("3")) {
					auxInvoiceLine.setPorcentaje("ICE 15%");
				} else if (auxTipoImpuesto.equals("3")) {
					auxInvoiceLine.setPorcentaje("ICE " + lineas.get(i).getPorcentajeimpuesto() + "%");
				} else if (auxTipoImpuesto.equals("5")) {
					auxInvoiceLine.setPorcentaje("IRBPNR " + lineas.get(i).getPorcentajeimpuesto() + "%");
				} else {
					auxInvoiceLine.setPorcentaje("EXENTO");
				}
				
				List<String> auxCTaxId = this.bddPostgres.getCTaxId(auxInvoiceLine.getPorcentaje(), lineas.get(i).getCodigoimpuesto(), connection);
				
				if (auxCTaxId.isEmpty()) {
					this.mensaje = "No hay el impuesto " + auxInvoiceLine.getPorcentaje() + " con codigo " + lineas.get(i).getCodigoimpuesto() 
							+ " en la bdd del sistema de facturaci�n electr�nica de ATRUMS por favor comuniquese con Soporte.";
					connection.rollback();
					return false;
				} else if (auxCTaxId.size() != 1) {
					this.mensaje = "Hay m�s de un impuesto " + auxInvoiceLine.getPorcentaje() + " en la bdd del sistema de facturaci�n electr�nica de ATRUMS por favor comuniquese con Soporte.";
					connection.rollback();
					return false;
				} else {
					auxInvoiceLine.setcTaxId(auxCTaxId.get(0));
					auxCTaxId = null;
				}
				
				auxInvoiceLine.setcInvoiceLineId(bddPostgres.putCInvoiceLineId(auxInvoiceLine, connection));
				
				if (auxInvoiceLine.getcInvoiceLineId() == null) {
					this.mensaje = "No se guardo la linea del producto c�digo Descuento en la bdd del sistema de facturaci�n electr�nica de ATRUMS por favor intentelo m�s tarde, o comuniquese con Soporte.";
					connection.rollback();
					return false;
				}
				
				auxInvoiceLineas.add(auxInvoiceLine);
			}
			
			if (!this.bddPostgres.procesarDocumento(this.jsonFactura, auxInvoiceLineas, connection)) {
				this.mensaje = "# documento: " + this.bddPostgres.getAuxInvoice().getDocumentnoOpen() + ", " + this.bddPostgres.getMensajeError() + ", en la bdd del sistema de facturaci�n electr�nica de ATRUMS por favor intentelo m�s tarde, o comuniquese con Soporte.";
				connection.rollback();
				return false;
			}
			
			double totalPagos = 0;
			
			List<Pago> pagos = this.jsonFactura.getPagos();
			
			for (int i=0; i<pagos.size(); i++) {
				Pago pago = new Pago();
				
				String metodoPago = "";
				
				if (pagos.get(i).getMetodoPago().equals("SIN UTILIZACION DEL SISTEMA FINANCIERO")) {
					metodoPago = "Efectivo";
				} else if (pagos.get(i).getMetodoPago().equals("COMPENSACI�N DE DEUDAS") || 
						pagos.get(i).getMetodoPago().equals("COMPENSACION DE DEUDAS")) {
					metodoPago = "Deudas";
				} else if (pagos.get(i).getMetodoPago().equals("TARJETA DE D�BITO") || 
						pagos.get(i).getMetodoPago().equals("TARJETA DE DEBITO")) {
					metodoPago = "Tarjeta de D�bito";
				} else if (pagos.get(i).getMetodoPago().equals("DINERO ELECTR�NICO") || 
						pagos.get(i).getMetodoPago().equals("DINERO ELECTRONICO")) {
					metodoPago = "Dinero Electr�nico";
				} else if (pagos.get(i).getMetodoPago().equals("TARJETA PREPAGO")) {
					metodoPago = "Tarjeta Prepago";
				} else if (pagos.get(i).getMetodoPago().equals("TARJETA DE CR�DITO") || 
						pagos.get(i).getMetodoPago().equals("TARJETA DE CREDITO")) {
					metodoPago = "Tarjeta de Cr�dito";
				} else if (pagos.get(i).getMetodoPago().equals("OTROS CON UTILIZACI�N DEL SISTEMA FINANCIERO") || 
						pagos.get(i).getMetodoPago().equals("OTROS CON UTILIZACION DEL SISTEMA FINANCIERO")) {
					metodoPago = "Otro";
				} else if (pagos.get(i).getMetodoPago().equals("ENDOSO DE T�TULOS") || 
						pagos.get(i).getMetodoPago().equals("ENDOSO DE TITULOS")) {
					metodoPago = "Endoso";
				}
				
				List<String> auxFinPaymentmethodId = this.bddPostgres.getFinPaymentmethodId(metodoPago.toString(), connection);
				
				if (auxFinPaymentmethodId.isEmpty()) {
					this.mensaje = "Tiene un metodo de pago " + pagos.get(i).getMetodoPago().toString() +  " : " + pagos.get(i).getTotalPago().toString() + " que no hay en la bdd del sistema de facturaci�n electr�nica de ATRUMS por favor intentelo m�s tarde, o comuniquese con Soporte.";
					auxFinPaymentmethodId = null;
				} else {
					pago.setFinPaymentmethodId(auxFinPaymentmethodId.get(0));
					
					List<String> auxFinPaymentScheduleId = this.bddPostgres.getFinPaymentScheduleId(connection);
					
					if (auxFinPaymentScheduleId.isEmpty()) {
						this.mensaje = "No hay plan de pagos en el documento en la bdd del sistema de facturaci�n electr�nica de ATRUMS por favor intentelo m�s tarde, o comuniquese con Soporte.";
						connection.rollback();
						return false;
					}else{
						pago.setFinPaymentScheduleId(auxFinPaymentScheduleId.get(0));
						pago.setAmount(pagos.get(i).getTotalPago().toString().replaceAll(",", "."));
						pago.setReferencia("M�todo: " + metodoPago);
						
						totalPagos = totalPagos + Double.valueOf(pago.getAmount());
						
						pago.setFinPaymentId(bddPostgres.putFinPaymentId(pago, connection));
						
						if (pago.getFinPaymentId() == null) {
							this.mensaje = "No se ingreso el pago en la bdd del sistema de facturaci�n electr�nica de ATRUMS por favor intentelo m�s tarde, o comuniquese con Soporte.";
							connection.rollback();
							return false;
						} else {
							pago.setFinPaymentDetailId(bddPostgres.putFinPaymentDetailId(pago, connection));
							
							if (pago.getFinPaymentDetailId() == null) {
								this.mensaje = "No se ingreso el pago en la bdd del sistema de facturaci�n electr�nica de ATRUMS por favor intentelo m�s tarde, o comuniquese con Soporte.";
								connection.rollback();
								return false;
							} else {
								if (!this.bddPostgres.updateFinPaymentScheduledetailId(pago, connection)) {
									this.mensaje = "No se ingreso el pago en la bdd del sistema de facturaci�n electr�nica de ATRUMS por favor intentelo m�s tarde, o comuniquese con Soporte.";
									connection.rollback();
									return false;
								} else {
									if (!this.bddPostgres.procesarPago(pago, connection)) {
										this.mensaje = "No se ingreso el pago en la bdd del sistema de facturaci�n electr�nica de ATRUMS por favor intentelo m�s tarde, o comuniquese con Soporte.";
										connection.rollback();
										return false;
									}
								}
							}
						}
						
						auxFinPaymentScheduleId = null;
					}
					
					auxFinPaymentmethodId = null;
				}
			}
			
			double auxtotal = Double.valueOf(this.bddPostgres.getAuxInvoice().getTotal());
			boolean isPagado = auxtotal == totalPagos? true : false;
			
			if (!this.bddPostgres.terminarFactura(isPagado, totalPagos, connection)) {
				this.mensaje = "No se ingreso el pago en la bdd del sistema de facturaci�n electr�nica de ATRUMS por favor intentelo m�s tarde, o comuniquese con Soporte.";
				connection.rollback();
				return false;
			} else {
				connection.commit();
				
				/*List<PocEmail> pocEmails = this.bddPostgres.getPocEmaild(connection);
				
				if (pocEmails.isEmpty()) {
					log.warn("Hay configurci�n de Correo");
				} else if (pocEmails.size() > 1) {
					log.warn("Hay demasiadas configuraciones de Correo para el mismo cliente");
				} else {
					this.bddPostgres.getAuxInvoice().setPocEmail(pocEmails.get(0));
					
					try {
						ExecutorService exServiceWeb = Executors.newFixedThreadPool(1);
				        Runnable runnableWeb = new OperacionesEnvioEmail(this.bddPostgres.getAuxInvoice(), this.jsonFactura.getEmailcliente(), 
								this.jsonFactura.getCliente(), 
								"01", 
								this.dataSource);
				        exServiceWeb.execute(runnableWeb);
				        
				        exServiceWeb.shutdown();
					} catch (Exception ex) {
						// TODO: handle exception
						StringWriter stack = new StringWriter();
						ex.printStackTrace(new PrintWriter(stack));
						log.warn("Caught exception; decorating with appropriate status template : " + stack.toString());
					}
				}*/
				
				return true;
			}
		} catch (Exception ex) {
			// TODO: handle exception
			StringWriter stack = new StringWriter();
			ex.printStackTrace(new PrintWriter(stack));
			log.warn("Caught exception; decorating with appropriate status template : " + stack.toString());
		} /*finally {
			try {if (connection != null) connection.close();} catch (SQLException ex) {}
		}*/
		
		return false;
	}
	
	public boolean procesarNotaOpenbravo() {
		List<InvoiceLine> auxInvoiceLineas = new ArrayList<InvoiceLine>();
		
		try {
			if (this.connection == null) {
				this.mensaje = "Hay fallas con la bdd del sistema de facturaci�n electr�nica de ATRUMS por favor intentelo m�s tarde, o comuniquese con Soporte.";
				return false;
			}
			
			this.bddPostgres.setAuxInvoice(this.docAutInvoice);
			
			this.bddPostgres.getAuxInvoice().setClaveacceso(this.jsonNotaCredito.getClaveacceso());
			
			DateFormat formatori = new SimpleDateFormat("ddMMyyyy");
			DateFormat formatmod = new SimpleDateFormat("dd/MM/yyyy");
			
			Date auxDate = formatori.parse(this.jsonNotaCredito.getClaveacceso().substring(0, 8));
			
			this.bddPostgres.getAuxInvoice().setDateinvoiced(formatmod.format(auxDate));
			
			this.bddPostgres.getAuxInvoice().setAutorization(this.jsonNotaCredito.getAutorizacion());
			this.bddPostgres.getAuxInvoice().setDocXML(this.docxml);
			this.bddPostgres.getAuxInvoice().setFileXML(this.filexml);
			
			if (!this.bddPostgres.updateInvoice("PD", this.connection)) {
				this.connection.rollback();
				this.mensaje = "Hay fallas con la bdd del sistema de facturaci�n electr�nica de ATRUMS por favor intentelo m�s tarde, o comuniquese con Soporte.";
				return false;
			}
			
			List<Linea> lineas = this.jsonNotaCredito.getLineas();
			
			int linea = 0;
			
			for(int i=0; i<lineas.size(); i++){
				InvoiceLine auxInvoiceLine = new InvoiceLine();
				
				linea = linea + 10;
				auxInvoiceLine.setLinea(linea);
				
				List<String> auxMProductId = this.bddPostgres.getMProductId(lineas.get(i).getCodigoProducto(), connection);
				
				if (auxMProductId.isEmpty()) {
					List<String> auxProductCatergoryId = this.bddPostgres.getMProductCategoryId(this.configuracion.getCategoriaproducto(), connection); 
					
					if (auxProductCatergoryId.isEmpty()) {
						auxInvoiceLine.setmProductCategoryId(bddPostgres.putMProductCategoryId(this.configuracion.getCategoriaproducto(), connection));
						
						if (auxInvoiceLine.getmProductCategoryId() == null) {
							this.mensaje = "No se guardo la categoria del producto c�digo " + this.configuracion.getCategoriaproducto() + " en la bdd del sistema de facturaci�n electr�nica de ATRUMS por favor intentelo m�s tarde, o comuniquese con Soporte.";
							connection.rollback();
							return false;
						}
					} else if (auxProductCatergoryId.size() != 1) {
						this.mensaje = "Hay m�s de una categor�a del producto " + configuracion.getCategoriaproducto() + " en la bdd del sistema de facturaci�n electr�nica de ATRUMS por favor intentelo m�s tarde, o comuniquese con Soporte.";
						connection.rollback();
						return false;
					} else {
						auxInvoiceLine.setmProductCategoryId(auxProductCatergoryId.get(0));
						auxProductCatergoryId = null;
					}
					
					DocumentoLinea auxLinea = new DocumentoLinea();
					auxLinea.setItemcdgodtfc(lineas.get(i).getCodigoProducto());
					auxLinea.setDtfcdscr(lineas.get(i).getNombreProducto().toString().replaceAll("'",""));
					
					auxInvoiceLine.setmProductId(bddPostgres.putMProductId(auxInvoiceLine.getmProductCategoryId(), auxLinea, connection));
					
					if (auxInvoiceLine.getmProductId() == null) {
						this.mensaje = "No se guardo el producto con este c�digo " + lineas.get(i).getCodigoProducto() + " en la bdd del sistema de facturaci�n electr�nica de ATRUMS por favor intentelo m�s tarde, o comuniquese con Soporte.";
						connection.rollback();
						return false;
					}
					
				} else if (auxMProductId.size() != 1) {
					this.mensaje = "Hay m�s de un producto con el mismo c�digo en la bdd del sistema de facturaci�n electr�nica de ATRUMS por favor intentelo m�s tarde, o comuniquese con Soporte.";
					connection.rollback();
					return false;
				} else {
					auxInvoiceLine.setmProductId(auxMProductId.get(0));
					auxMProductId = null;
				}
				
				auxInvoiceLine.setCantidad(lineas.get(i).getCantidad());
				auxInvoiceLine.setPrecioUnitario(lineas.get(i).getPrecio());
				auxInvoiceLine.setDescuento(lineas.get(i).getDescuento());
				
				DecimalFormat format2 = new DecimalFormat("0.00");
				DecimalFormat format3 = new DecimalFormat("0.000");
				double auxPorcentajeDescuento = 0;
				
				if (Double.valueOf(auxInvoiceLine.getPrecioUnitario()) != 0) {
					auxPorcentajeDescuento = Double.valueOf(auxInvoiceLine.getDescuento()) * (100/(Double.valueOf(auxInvoiceLine.getPrecioUnitario()) * Double.valueOf(auxInvoiceLine.getCantidad())));
				} 
				
				auxInvoiceLine.setPorcentajeDescuento(format3.format(auxPorcentajeDescuento).replace(",", "."));
				auxInvoiceLine.setTotalLineaSinImpuestos(lineas.get(i).getTotallinea());
				auxInvoiceLine.setValor(lineas.get(i).getImpuestolinea());
				
				double auxImpuesto = Double.valueOf(auxInvoiceLine.getValor());
				double auxTotal = Double.valueOf(auxInvoiceLine.getTotalLineaSinImpuestos());
				
				auxInvoiceLine.setTotalLinea(format2.format(auxTotal + auxImpuesto).replace(",", "."));
				
				String auxTipoImpuesto = "";
				
				if (lineas.get(i).getTipoimpuesto().equals("IVA")) {
					auxTipoImpuesto = "2";
				} else if (lineas.get(i).getTipoimpuesto().equals("ICE")) {
					auxTipoImpuesto = "3";
				} else if (lineas.get(i).getTipoimpuesto().equals("IRBPNR")) {
					auxTipoImpuesto = "5";
				} else {
					auxTipoImpuesto = lineas.get(i).getTipoimpuesto();
				}
				
				if (lineas.get(i).getPorcentajeimpuesto().equals("14") && auxTipoImpuesto.equals("2")) {
					auxInvoiceLine.setPorcentaje("IVA 14%");
				} else if (lineas.get(i).getPorcentajeimpuesto().equals("12") && auxTipoImpuesto.equals("2")) {
					auxInvoiceLine.setPorcentaje("IVA 12%");
				} else if (lineas.get(i).getPorcentajeimpuesto().equals("0") && auxTipoImpuesto.equals("2")) {
					auxInvoiceLine.setPorcentaje("IVA 0%");
				} else if (auxTipoImpuesto.equals("2")) {
					auxInvoiceLine.setPorcentaje("IVA " + lineas.get(i).getPorcentajeimpuesto() + "%");
				} else if (lineas.get(i).getPorcentajeimpuesto().equals("15") && auxTipoImpuesto.equals("3")) {
					auxInvoiceLine.setPorcentaje("ICE 15%");
				} else if (auxTipoImpuesto.equals("3")) {
					auxInvoiceLine.setPorcentaje("ICE " + lineas.get(i).getPorcentajeimpuesto() + "%");
				} else if (auxTipoImpuesto.equals("5")) {
					auxInvoiceLine.setPorcentaje("IRBPNR " + lineas.get(i).getPorcentajeimpuesto() + "%");
				} else {
					auxInvoiceLine.setPorcentaje("EXENTO");
				}
				
				List<String> auxCTaxId = this.bddPostgres.getCTaxId(auxInvoiceLine.getPorcentaje(), lineas.get(i).getCodigoimpuesto(), connection);
				
				if (auxCTaxId.isEmpty()) {
					this.mensaje = "No hay el impuesto " + auxInvoiceLine.getPorcentaje() + " con codigo " + lineas.get(i).getCodigoimpuesto()
							+ " en la bdd del sistema de facturaci�n electr�nica de ATRUMS por favor comuniquese con Soporte.";
					connection.rollback();
					return false;
				} else if (auxCTaxId.size() != 1) {
					this.mensaje = "Hay m�s de un impuesto " + auxInvoiceLine.getPorcentaje() + " en la bdd del sistema de facturaci�n electr�nica de ATRUMS por favor comuniquese con Soporte.";
					connection.rollback();
					return false;
				} else {
					auxInvoiceLine.setcTaxId(auxCTaxId.get(0));
					auxCTaxId = null;
				}
				
				auxInvoiceLine.setcInvoiceLineId(bddPostgres.putCInvoiceLineId(auxInvoiceLine, connection));
				
				if (auxInvoiceLine.getcInvoiceLineId() == null) {
					this.mensaje = "No se guardo la linea del producto c�digo Descuento en la bdd del sistema de facturaci�n electr�nica de ATRUMS por favor intentelo m�s tarde, o comuniquese con Soporte.";
					connection.rollback();
					return false;
				}
				
				auxInvoiceLineas.add(auxInvoiceLine);
			}
			
			if (!this.bddPostgres.procesarDocumento(this.jsonNotaCredito, auxInvoiceLineas, connection)) {
				this.mensaje = "# documento: " + this.bddPostgres.getAuxInvoice().getDocumentnoOpen() + ", " + this.bddPostgres.getMensajeError() + ", en la bdd del sistema de facturaci�n electr�nica de ATRUMS por favor intentelo m�s tarde, o comuniquese con Soporte.";
				connection.rollback();
				return false;
			} else {
				connection.commit();
				
				/*List<PocEmail> pocEmails = this.bddPostgres.getPocEmaild(connection);
				
				if (pocEmails.isEmpty()) {
					log.warn("Hay configurci�n de Correo");
				} else if (pocEmails.size() > 1) {
					log.warn("Hay demasiadas configuraciones de Correo para el mismo cliente");
				} else {
					this.bddPostgres.getAuxInvoice().setPocEmail(pocEmails.get(0));
					
					try {
						ExecutorService exServiceWeb = Executors.newFixedThreadPool(1);
				        Runnable runnableWeb = new OperacionesEnvioEmail(this.bddPostgres.getAuxInvoice(), this.jsonNotaCredito.getEmailcliente(), 
								this.jsonNotaCredito.getCliente(), 
								"04", 
								this.dataSource);
				        exServiceWeb.execute(runnableWeb);
				        
				        exServiceWeb.shutdown();
					} catch (Exception ex) {
						// TODO: handle exception
						StringWriter stack = new StringWriter();
						ex.printStackTrace(new PrintWriter(stack));
						log.warn("Caught exception; decorating with appropriate status template : " + stack.toString());
					}
				}*/
				
				return true;
			}
		} catch (Exception ex) {
			// TODO: handle exception
			StringWriter stack = new StringWriter();
			ex.printStackTrace(new PrintWriter(stack));
			log.warn("Caught exception; decorating with appropriate status template : " + stack.toString());
		} /*finally {
			try {if (connection != null) connection.close();} catch (SQLException ex) {}
		}*/
		
		return false;
	}
}
